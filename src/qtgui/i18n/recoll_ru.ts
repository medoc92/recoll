<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" sourcelanguage="en" language="ru_RU">
<context>
    <name>ActSearchDLG</name>
    <message>
        <location filename="../actsearch.ui" line="14">
        </location>
        <source>Menu search</source>
        <translation>Меню поиска</translation>
    </message>
</context>
<context>
    <name>AdvSearch</name>
    <message>
        <location filename="../advsearch_w.cpp" line="109">
        </location>
        <source>All clauses</source>
        <translation>всем условиям</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="110">
        </location>
        <source>Any clause</source>
        <translation>любому условию</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="180">
        </location>
        <source>text</source>
        <translation>текст</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="181">
        </location>
        <source>texts</source>
        <translation>тексты</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="183">
        </location>
        <source>spreadsheet</source>
        <translation>таблица</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="184">
        </location>
        <source>spreadsheets</source>
        <translation>таблицы</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="186">
        </location>
        <location filename="../advsearch_w.cpp" line="187">
        </location>
        <source>presentation</source>
        <translation>презентация</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="189">
        </location>
        <location filename="../advsearch_w.cpp" line="190">
        </location>
        <source>media</source>
        <translation>медиа</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="192">
        </location>
        <location filename="../advsearch_w.cpp" line="193">
        </location>
        <source>message</source>
        <translation>сообщение</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="195">
        </location>
        <location filename="../advsearch_w.cpp" line="196">
        </location>
        <source>other</source>
        <translation>прочее</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="211">
        </location>
        <location filename="../advsearch_w.cpp" line="214">
        </location>
        <location filename="../advsearch_w.cpp" line="221">
        </location>
        <location filename="../advsearch_w.cpp" line="224">
        </location>
        <source>Advanced Search</source>
        <translation>Сложный поиск</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="211">
        </location>
        <location filename="../advsearch_w.cpp" line="221">
        </location>
        <source>Load next stored search</source>
        <translation>Загрузить следующий сохранённый запрос</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="214">
        </location>
        <location filename="../advsearch_w.cpp" line="224">
        </location>
        <source>Load previous stored search</source>
        <translation>Загрузить предыдущий сохранённый запрос</translation>
    </message>
    <message>
        <location filename="../advsearch_w.cpp" line="434">
        </location>
        <source>Bad multiplier suffix in size filter</source>
        <translation>Неверный множитель в фильтре размера</translation>
    </message>
</context>
<context>
    <name>AdvSearchBase</name>
    <message>
        <location filename="../advsearch.ui" line="14">
        </location>
        <source>Advanced search</source>
        <translation>Сложный поиск</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="27">
        </location>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="46">
        </location>
        <location filename="../advsearch.ui" line="74">
        </location>
        <source>All non empty fields on the right will be combined with AND (&quot;All clauses&quot; choice) or OR (&quot;Any clause&quot; choice) conjunctions. &lt;br&gt;&quot;Any&quot; &quot;All&quot; and &quot;None&quot; field types can accept a mix of simple words, and phrases enclosed in double quotes.&lt;br&gt;Fields with no data are ignored.</source>
        <translation>Все заполненные поля справа будут объединены логическим И («Все условия») или ИЛИ («Любое условие»). &lt;br&gt;В полях типа «Любые», «Все» или «Без» допустимы сочетания простых слов и фразы, заключённые в двойные кавычки.&lt;br&gt;Пустые поля игнорируются.</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="55">
        </location>
        <source>Search for &lt;br&gt;documents&lt;br&gt;satisfying:</source>
        <translation>Искать &lt;br&gt;документы,&lt;br&gt;удовлетворяющие:</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="103">
        </location>
        <source>Delete clause</source>
        <translation>Удалить условие</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="119">
        </location>
        <source>Add clause</source>
        <translation>Добавить условие</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="171">
        </location>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="185">
        </location>
        <location filename="../advsearch.ui" line="243">
        </location>
        <source>Check this to enable filtering on dates</source>
        <translation>Включить фильтрование по дате</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="188">
        </location>
        <source>Filter dates</source>
        <translation>Фильтровать по дате</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="195">
        </location>
        <location filename="../advsearch.ui" line="253">
        </location>
        <source>From</source>
        <translation>Из</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="209">
        </location>
        <location filename="../advsearch.ui" line="267">
        </location>
        <source>To</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="246">
        </location>
        <source>Filter birth dates</source>
        <translation>Фильтровать по дате рождения</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="301">
        </location>
        <source>Check this to enable filtering on sizes</source>
        <translation>Включить фильтрование по размеру</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="304">
        </location>
        <source>Filter sizes</source>
        <translation>Фильтровать по размеру</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="311">
        </location>
        <source>Minimum size. You can use k/K,m/M,g/G as multipliers</source>
        <translation>Минимальный размер. Допускается использование множителей к/К, м/М, г/Г</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="314">
        </location>
        <source>Min. Size</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="328">
        </location>
        <source>Maximum size. You can use k/K,m/M,g/G as multipliers</source>
        <translation>Максимальный размер. Допускается использование множителей к/К, м/М, г/Г</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="331">
        </location>
        <source>Max. Size</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="367">
        </location>
        <source>Check this to enable filtering on file types</source>
        <translation>Фильтровать по типам файлов</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="370">
        </location>
        <source>Restrict file types</source>
        <translation>Ограничить типы файлов</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="386">
        </location>
        <source>Check this to use file categories instead of raw mime types</source>
        <translation>Использовать категории, а не типы MIME</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="389">
        </location>
        <source>By categories</source>
        <translation>По категориям</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="399">
        </location>
        <source>Save as default</source>
        <translation>Сделать параметром по умолчанию</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="418">
        </location>
        <source>Searched file types</source>
        <translation>Искать среди</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="454">
        </location>
        <source>All ----&gt;</source>
        <translation>Все ----&gt;</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="467">
        </location>
        <source>Sel -----&gt;</source>
        <translation>Выделенные ----&gt;</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="480">
        </location>
        <source>&lt;----- Sel</source>
        <translation>&lt;----- Выделенные</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="493">
        </location>
        <source>&lt;----- All</source>
        <translation>&lt;----- Все</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="510">
        </location>
        <source>Ignored file types</source>
        <translation>Игнорируемые типы файлов</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="566">
        </location>
        <source>Enter top directory for search</source>
        <translation>Указать имя каталога верхнего уровня для поиска</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="585">
        </location>
        <source>Browse</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="595">
        </location>
        <source>Restrict results to files in subtree:</source>
        <translation>Ограничить результаты поиска файлами в подкаталоге:</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="605">
        </location>
        <source>Invert</source>
        <translation>Обратить</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="620">
        </location>
        <source>Start Search</source>
        <translation>Начать поиск</translation>
    </message>
    <message>
        <location filename="../advsearch.ui" line="627">
        </location>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>ConfIndexW</name>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="209">
        </location>
        <source>Can&apos;t write configuration file</source>
        <translation>Невозможно записать файл конфигурации</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="220">
        </location>
        <source>Global parameters</source>
        <translation>Общие параметры</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="225">
        </location>
        <source>Local parameters</source>
        <translation>Локальные параметры</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="227">
        </location>
        <source>Web history</source>
        <translation>История в веб</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="230">
        </location>
        <source>Search parameters</source>
        <translation>Параметры поиска</translation>
    </message>
    <message>
        <source>Top directories</source>
        <translation type="vanished">Каталоги верхнего уровня</translation>
    </message>
    <message>
        <source>The list of directories where recursive indexing starts. Default: your home.</source>
        <translation type="vanished">Список каталогов, где начинается рекурсивное индексирование. По умолчанию: домашний каталог.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="242">
        </location>
        <source>Skipped paths</source>
        <translation>Пропущенные пути</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="243">
        </location>
        <source>These are pathnames of directories which indexing will not enter.&lt;br&gt;Path elements may contain wildcards. The entries must match the paths seen by the indexer (e.g.: if topdirs includes &apos;/home/me&apos; and &apos;/home&apos; is actually a link to &apos;/usr/home&apos;, a correct skippedPath entry would be &apos;/home/me/tmp*&apos;, not &apos;/usr/home/me/tmp*&apos;)</source>
        <translation>Полный путь к директории, которая не будет затрагиваться при индексировании. &lt;br&gt;Может содержать маски. Записи должны совпадать с путями, которые видит индексатор (например, если topdirs включает «/home/me», а «/home» на самом деле ведёт к «/usr/home», правильной записью skippedPath будет «/home/me/tmp*», а не «/usr/home/me/tmp*»)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="259">
        </location>
        <source>Stemming languages</source>
        <translation>Языки со словоформами</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="260">
        </location>
        <source>The languages for which stemming expansion dictionaries will be built.&lt;br&gt;See the Xapian stemmer documentation for possible values. E.g. english, french, german...</source>
        <translation>Языки, для которых будут составлены словари словоформ.&lt;br&gt;Доступные значения описаны в документации к Xapian (например, english, french, russian...)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="266">
        </location>
        <source>Log file name</source>
        <translation>Имя файла журнала</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="267">
        </location>
        <source>The file where the messages will be written.&lt;br&gt;Use &apos;stderr&apos; for terminal output</source>
        <translation>Файл, куда будут записываться сообщения.&lt;br&gt;Используйте &apos;stderr&apos; для вывода в терминал</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="271">
        </location>
        <source>Log verbosity level</source>
        <translation>Уровень подробности журнала</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="272">
        </location>
        <source>This value adjusts the amount of messages,&lt;br&gt;from only errors to a lot of debugging data.</source>
        <translation>Это значение определяет подробность сообщений,&lt;br&gt;от ошибок до отладочных данных.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="276">
        </location>
        <source>Indexer log file name</source>
        <translation>Имя файла журнала индексатора</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="277">
        </location>
        <source>If empty, the above log file name value will be used. It may useful to have a separate log for diagnostic purposes because the common log will be erased when&lt;br&gt;the GUI starts up.</source>
        <translation>Если поле пустое, будет использовано имя файла выще. Отдельный файл с журналом для отладки может быть полезен, т.к. общий журнал&lt;br&gt;при старте интерфейса будет затёрт.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="283">
        </location>
        <source>Index flush megabytes interval</source>
        <translation>Интервал сброса данных индекса (МБ)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="284">
        </location>
        <source>This value adjust the amount of data which is indexed between flushes to disk.&lt;br&gt;This helps control the indexer memory usage. Default 10MB </source>
        <translation>Это значение определяет количество данных, индексируеммых между сбросами на диск.&lt;br&gt;Помогает контролировать использование памяти индексатором. Значение по умолчанию: 10МБ </translation>
    </message>
    <message>
        <source>Disk full threshold percentage at which we stop indexing&lt;br&gt;E.g. 90% to stop at 90% full, 0 or 100 means no limit)</source>
        <translation type="vanished">Степень заполнения диска в процентах, при которой прекратится индексирование&lt;br&gt;Например, 90% для останова на 90% заполнения; 0 или 100 снимает ограничение</translation>
    </message>
    <message>
        <source>Disk full threshold percentage at which we stop indexing&lt;br&gt;(E.g. 90% to stop at 90% full, 0 or 100 means no limit)</source>
        <translation type="vanished">Степень заполнения диска в процентах, при которой прекратится индексирование&lt;br&gt;Например, 90% для останова на 90% заполнения; 0 или 100 снимает ограничение</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="237">
        </location>
        <source>Start folders</source>
        <translation type="unfinished">Начать папки</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="238">
        </location>
        <source>The list of folders/directories to be indexed. Sub-folders will be recursively processed. Default: your home.</source>
        <translation type="unfinished">Список папок/каталогов для индексации. Подпапки будут обрабатываться рекурсивно. По умолчанию: ваш домашний каталог.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="290">
        </location>
        <source>Disk full threshold percentage at which we stop indexing&lt;br&gt;(E.g. 90 to stop at 90% full, 0 or 100 means no limit)</source>
        <translation type="unfinished">Процент заполнения диска, при достижении которого мы прекращаем индексацию (например, 90 для остановки на 90% заполнения, 0 или 100 означает отсутствие ограничения)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="292">
        </location>
        <source>This is the percentage of disk usage - total disk usage, not index size - at which indexing will fail and stop.&lt;br&gt;The default value of 0 removes any limit.</source>
        <translation>Процент занятого пространства на диске (в целом, не только индексом), при котором индексирование завершится ошибкой и прекратится.&lt;br&gt;По умолчанию значение 0 снимает ограничение.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="298">
        </location>
        <source>No aspell usage</source>
        <translation>Не использовать aspell</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="299">
        </location>
        <source>(by default, aspell suggests mispellings when a query has no results).</source>
        <translation>(по умолчанию aspell подсказывает об опечатках, когда запрос не даёт результатов)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="300">
        </location>
        <source>Disables use of aspell to generate spelling approximation in the term explorer tool.&lt;br&gt; Useful if aspell is absent or does not work. </source>
        <translation>Отключает использование aspell для создания вариантов написания в обозревателе терминов.&lt;br&gt; Полезно, если aspell отсутствует или не работает.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="305">
        </location>
        <source>Aspell language</source>
        <translation>Язык aspell</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="306">
        </location>
        <source>The language for the aspell dictionary. The values are are 2-letter language codes, e.g. &apos;en&apos;, &apos;fr&apos; ...&lt;br&gt;If this value is not set, the NLS environment will be used to compute it, which usually works. To get an idea of what is installed on your system, type &apos;aspell config&apos; and look for .dat files inside the &apos;data-dir&apos; directory.</source>
        <translation>Язык словаря aspell в виде двухбуквенного кода, например, &apos;en&apos;, &apos;fr&apos;, &apos;ru&apos;...&lt;br&gt;Если значение не установлено, будет предпринята попытка вывести его из локали, что обычно срабатывает. Чтобы посмотреть, что установлено на вашей системе, наберите &apos;aspell config&apos; и поищите .dat-файлы в каталоге, указанном как &apos;data-dir&apos;.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="317">
        </location>
        <source>Database directory name</source>
        <translation>Каталог базы данных</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="318">
        </location>
        <source>The name for a directory where to store the index&lt;br&gt;A non-absolute path is taken relative to the configuration directory. The default is &apos;xapiandb&apos;.</source>
        <translation>Имя каталога, в котором хранится индекс&lt;br&gt;Путь указывается относительно каталога конфигурации и не является абсолютным. По умолчанию: «xapiandb».</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="322">
        </location>
        <source>Unac exceptions</source>
        <translation>Исключения unac</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="323">
        </location>
        <source>&lt;p&gt;These are exceptions to the unac mechanism which, by default, removes all diacritics, and performs canonic decomposition. You can override unaccenting for some characters, depending on your language, and specify additional decompositions, e.g. for ligatures. In each space-separated entry, the first character is the source one, and the rest is the translation.</source>
        <translation>Это исключения для механизма unac, который по умолчанию отбрасывает все диакритические знаки и проводит каноническую декомпозицию. Можно переопределить механизм удаления надстрочных знаков для отдельных символов или добавить правила декомпозиции (например, для лигатур). В каждой отделённой запятой записи первый символ является исходным, а остальные — его интерпретации.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="340">
        </location>
        <source>Process the Web history queue</source>
        <translation>Обработка очереди истории веб-поиска</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="341">
        </location>
        <source>Enables indexing Firefox visited pages.&lt;br&gt;(you need also install the Firefox Recoll plugin)</source>
        <translation>Включает индексацию посещённых страниц в Firefox.&lt;br&gt;(требуется установка плагина Recoll)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="345">
        </location>
        <source>Web page store directory name</source>
        <translation>Имя каталога с сохранёнными веб-страницами</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="346">
        </location>
        <source>The name for a directory where to store the copies of visited web pages.&lt;br&gt;A non-absolute path is taken relative to the configuration directory.</source>
        <translation>Имя каталога хранения просмотренных веб-страниц.&lt;br&gt;Путь указывается относительно каталога конфигурации и не является абсолютным.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="351">
        </location>
        <source>Max. size for the web store (MB)</source>
        <translation>Предельный размер веб-хранилища (МБ)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="352">
        </location>
        <source>Entries will be recycled once the size is reached.&lt;br&gt;Only increasing the size really makes sense because reducing the value will not truncate an existing file (only waste space at the end).</source>
        <translation>Записи будут удалены при достижении максимального размера хранилища.&lt;br&gt;Целесообразно только увеличивать размер, так как уменьшение значения не повлечёт усечение существующего файла (лишь перестанет использовать его хвост).</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="361">
        </location>
        <source>Page recycle interval</source>
        <translation>Интервал обновления страницы</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="362">
        </location>
        <source>&lt;p&gt;By default, only one instance of an URL is kept in the cache. This can be changed by setting this to a value determining at what frequency we keep multiple instances (&apos;day&apos;, &apos;week&apos;, &apos;month&apos;, &apos;year&apos;). Note that increasing the interval will not erase existing entries.</source>
        <translation>&lt;p&gt;По умолчанию в кэше хранится только один экземпляр ссылки. Это можно изменить в настройках, где указывается значение длительности хранения нескольких экземпляров (&apos;день&apos;, &apos;неделя&apos;, &apos;месяц&apos;, &apos;год&apos;). Учтите, что увеличение интервала не сотрёт уже существующие записи.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="375">
        </location>
        <source>Note: old pages will be erased to make space for new ones when the maximum size is reached. Current size: %1</source>
        <translation>На заметку: старые страницы будут стёрты, чтобы освободилось место для новых, когда будет достигнут предельный объём. Текущий размер: %1</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="381">
        </location>
        <source>Browser add-on download folder</source>
        <translation type="unfinished">Папка загрузки дополнений для браузера</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="382">
        </location>
        <source>Only set this if you set the &quot;Downloads subdirectory&quot; parameter in the Web browser add-on settings. &lt;br&gt;In this case, it should be the full path to the directory (e.g. /home/[me]/Downloads/my-subdir)</source>
        <translation type="unfinished">Установите это только в том случае, если вы установили параметр &quot;Подкаталог загрузок&quot; в настройках дополнения для веб-браузера. &lt;br&gt; В этом случае это должен быть полный путь к каталогу (например, /home/[me]/Downloads/my-subdir)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="395">
        </location>
        <source>Automatic diacritics sensitivity</source>
        <translation>Автоматический учёт диакритических знаков</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="396">
        </location>
        <source>&lt;p&gt;Automatically trigger diacritics sensitivity if the search term has accented characters (not in unac_except_trans). Else you need to use the query language and the &lt;i&gt;D&lt;/i&gt; modifier to specify diacritics sensitivity.</source>
        <translation>&lt;p&gt;Автоматически включает учёт диакритических знаков, если строка поиска содержит диакритические знаки (кроме unac_except_trans). В противном случае используйте язык запросов и модификатор &lt;i&gt;D&lt;/i&gt; для учёта диакритических знаков.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="403">
        </location>
        <source>Automatic character case sensitivity</source>
        <translation>Автоматический учёт регистра</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="404">
        </location>
        <source>&lt;p&gt;Automatically trigger character case sensitivity if the entry has upper-case characters in any but the first position. Else you need to use the query language and the &lt;i&gt;C&lt;/i&gt; modifier to specify character-case sensitivity.</source>
        <translation>&lt;p&gt;Автоматически включает учёт регистра, если строка поиска содержит заглавные буквы (кроме первой буквы). В противном случае используйте язык запросов и модификатор &lt;i&gt;C&lt;/i&gt; учёта регистра.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="413">
        </location>
        <source>Maximum term expansion count</source>
        <translation>Предельное число однокоренных слов</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="414">
        </location>
        <source>&lt;p&gt;Maximum expansion count for a single term (e.g.: when using wildcards). The default of 10 000 is reasonable and will avoid queries that appear frozen while the engine is walking the term list.</source>
        <translation>&lt;p&gt;Предельное число однокоренных слов для одного слова (например, при использовании масок). Значение по умолчанию в 10 000 является разумным и поможет избежать ситуаций, когда запрос кажется зависшим при переборе списка слов.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="421">
        </location>
        <source>Maximum Xapian clauses count</source>
        <translation>Предельное число Xapian-предложений</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="422">
        </location>
        <source>&lt;p&gt;Maximum number of elementary clauses we add to a single Xapian query. In some cases, the result of term expansion can be multiplicative, and we want to avoid using excessive memory. The default of 100 000 should be both high enough in most cases and compatible with current typical hardware configurations.</source>
        <translation>&lt;p&gt;Предельное число элементарных условий, добавляемых к запросу Xapian. В некоторых случаях результат поиска однокоренных слов может быть избыточным и занять слишком большой объём памяти. Значение по умолчанию в 100 000 достаточно для большинства случаев и подходит для современных аппаратных конфигураций.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="432">
        </location>
        <source>Store some GUI parameters locally to the index</source>
        <translation type="unfinished">Сохраните некоторые параметры GUI локально в индекс.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="433">
        </location>
        <source>&lt;p&gt;GUI settings are normally stored in a global file, valid for all indexes. Setting this parameter will make some settings, such as the result table setup, specific to the index</source>
        <translation type="unfinished">Настройки GUI обычно хранятся в общем файле, действительном для всех индексов. Установка этого параметра сделает некоторые настройки, такие как настройка таблицы результатов, специфичными для индекса.</translation>
    </message>
</context>
<context>
    <name>ConfSubPanelW</name>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="520">
        </location>
        <source>Only mime types</source>
        <translation>Только MIME-типы</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="521">
        </location>
        <source>An exclusive list of indexed mime types.&lt;br&gt;Nothing else will be indexed. Normally empty and inactive</source>
        <translation>Исчерпывающий перечень индексируемых типов MIME.&lt;br&gt;Другие типы индексироваться не будут. Обычно пуст и неактивен</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="530">
        </location>
        <source>Exclude mime types</source>
        <translation>Исключить MIME-типы</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="531">
        </location>
        <source>Mime types not to be indexed</source>
        <translation>Типы MIME, индексирование которых проводиться не будет</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="607">
        </location>
        <source>Max. compressed file size (KB)</source>
        <translation>Предельный размер сжатого файла (KB)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="608">
        </location>
        <source>This value sets a threshold beyond which compressedfiles will not be processed. Set to -1 for no limit, to 0 for no decompression ever.</source>
        <translation>Это значение устанавливает предельный размер сжатых файлов, которые будут обрабатываться. Значение -1 снимает ограничение, 0 отключает распаковку.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="618">
        </location>
        <source>Max. text file size (MB)</source>
        <translation>Предельный размер текстового файла (MB)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="619">
        </location>
        <source>This value sets a threshold beyond which text files will not be processed. Set to -1 for no limit. 
This is for excluding monster log files from the index.</source>
        <translation>Это значение устанавливает предельный размер текстовых файлов, которые будут обрабатываться. Значение -1 снимает ограничение.
Рекомендуется использовать для исключения файлов журнала большого размера из процесса индексирования.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="630">
        </location>
        <source>Text file page size (KB)</source>
        <translation>Pазмер страницы текстового файла (КБ)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="631">
        </location>
        <source>If this value is set (not equal to -1), text files will be split in chunks of this size for indexing.
This will help searching very big text  files (ie: log files).</source>
        <translation>Если это значение установлено (т.е. не равно -1), то при индексировании текстовые файлы разбиваются на блоки соответствующего размера.
Данный параметр полезен при выполнении поиска в очень больших текстовых файлах (например, файлах журналов).</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="642">
        </location>
        <source>Max. filter exec. time (s)</source>
        <translation>Пред. время работы фильтра (с)</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="643">
        </location>
        <source>External filters working longer than this will be aborted. This is for the rare case (ie: postscript) where a document could cause a filter to loop. Set to -1 for no limit.
</source>
        <translation>Работа внешних фильтров, длящаяся дольше указанного времени, будет прервана. Применяется для редких случаев (например, с фильтром postscript), когда возникает зацикливание фильтра при обработке какого-то документа. Установите значение -1, чтобы снять ограничение.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="678">
        </location>
        <source>Global</source>
        <translation>Общее</translation>
    </message>
</context>
<context>
    <name>ConfigSwitchDLG</name>
    <message>
        <location filename="../configswitch.ui" line="14">
        </location>
        <source>Switch to other configuration</source>
        <translation type="unfinished">Переключиться на другую конфигурацию</translation>
    </message>
</context>
<context>
    <name>ConfigSwitchW</name>
    <message>
        <location filename="../configswitch.cpp" line="44">
        </location>
        <source>Choose other</source>
        <translation type="unfinished">Выберите другой</translation>
    </message>
    <message>
        <location filename="../configswitch.cpp" line="114">
        </location>
        <source>Choose configuration directory</source>
        <translation type="unfinished">Выберите каталог конфигурации</translation>
    </message>
</context>
<context>
    <name>CronToolW</name>
    <message>
        <location filename="../crontool.ui" line="14">
        </location>
        <source>Cron Dialog</source>
        <translation>Настройка заданий Cron</translation>
    </message>
    <message>
        <location filename="../crontool.ui" line="28">
        </location>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; batch indexing schedule (cron) &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each field can contain a wildcard (*), a single numeric value, comma-separated lists (1,3,5) and ranges (1-7). More generally, the fields will be used &lt;span style=&quot; font-style:italic;&quot;&gt;as is&lt;/span&gt; inside the crontab file, and the full crontab syntax can be used, see crontab(5).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;For example, entering &lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;*&lt;/span&gt; in &lt;span style=&quot; font-style:italic;&quot;&gt;Days, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;12,19&lt;/span&gt; in &lt;span style=&quot; font-style:italic;&quot;&gt;Hours&lt;/span&gt; and &lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;15&lt;/span&gt; in &lt;span style=&quot; font-style:italic;&quot;&gt;Minutes&lt;/span&gt; would start recollindex every day at 12:15 AM and 7:15 PM&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A schedule with very frequent activations is probably less efficient than real time indexing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Индексирование &lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; по расписанию (cron) &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Каждое поле может содержать маску (*), единичное числовое значение, разделённый запятыми список (1,3,5) или диапазон чисел (1-7). Эти поля будут использованы &lt;span style=&quot; font-style:italic;&quot;&gt;как есть&lt;/span&gt; в файле crontab, также можно указать необходимые параметры в самом файле, см. crontab(5).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;Например, если ввести знак &lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;*&lt;/span&gt; в поле &lt;span style=&quot; font-style:italic;&quot;&gt;«Дни недели»&lt;/span&gt;, &lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;12,19&lt;/span&gt; — в поле &lt;span style=&quot; font-style:italic;&quot;&gt;«Часы»&lt;/span&gt; и &lt;span style=&quot; font-family:&apos;Courier New,courier&apos;;&quot;&gt;15&lt;/span&gt; — в поле &lt;span style=&quot; font-style:italic;&quot;&gt;«Минуты»&lt;/span&gt;, индексирование будет производиться ежедневно в 12:15 и 19:15.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Расписание с очень частыми запусками может оказаться менее эффективным, чем индексирование в реальном времени.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../crontool.ui" line="49">
        </location>
        <source>Days of week (* or 0-7, 0 or 7 is Sunday)</source>
        <translation>Дни недели (* или 0-7, 0 или 7 — воскресенье)</translation>
    </message>
    <message>
        <location filename="../crontool.ui" line="62">
        </location>
        <source>Hours (* or 0-23)</source>
        <translation>Часы (* или 0-23)</translation>
    </message>
    <message>
        <location filename="../crontool.ui" line="81">
        </location>
        <source>Minutes (0-59)</source>
        <translation>Minutes (0-59)</translation>
    </message>
    <message>
        <location filename="../crontool.ui" line="96">
        </location>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Click &lt;span style=&quot; font-style:italic;&quot;&gt;Disable&lt;/span&gt; to stop automatic batch indexing, &lt;span style=&quot; font-style:italic;&quot;&gt;Enable&lt;/span&gt; to activate it, &lt;span style=&quot; font-style:italic;&quot;&gt;Cancel&lt;/span&gt; to change nothing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Для остановки автоматического идексирования по расписанию нажмите &lt;span style=&quot; font-style:italic;&quot;&gt;«Выключить»&lt;/span&gt;, для запуска — &lt;span style=&quot; font-style:italic;&quot;&gt;«Включить»&lt;/span&gt;, для отмены внесённых изменений — &lt;span style=&quot; font-style:italic;&quot;&gt;«Отмена»&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../crontool.cpp" line="49">
        </location>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../crontool.cpp" line="50">
        </location>
        <source>Disable</source>
        <translation>Выключить</translation>
    </message>
    <message>
        <location filename="../crontool.cpp" line="62">
        </location>
        <source>It seems that manually edited entries exist for recollindex, cannot edit crontab</source>
        <translation>Похоже, что для recollindex есть вручную исправленные записи, редактирование crontab невозможно</translation>
    </message>
    <message>
        <location filename="../crontool.cpp" line="122">
        </location>
        <source>Error installing cron entry. Bad syntax in fields ?</source>
        <translation>Ошибка установки записи cron. Неверный синтаксис полей?</translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <location filename="../widgets/editdialog.ui" line="14">
        </location>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
</context>
<context>
    <name>EditTrans</name>
    <message>
        <source>Source path</source>
        <translation type="vanished">Исходный путь</translation>
    </message>
    <message>
        <location filename="../ptrans_w.cpp" line="111">
        </location>
        <source>Local path</source>
        <translation>Локальный путь</translation>
    </message>
    <message>
        <location filename="../ptrans_w.cpp" line="85">
        </location>
        <source>Config error</source>
        <translation>Ошибки конфигурации</translation>
    </message>
    <message>
        <location filename="../ptrans_w.cpp" line="54">
        </location>
        <source>Path in index</source>
        <translation type="unfinished">Путь в индексе</translation>
    </message>
    <message>
        <location filename="../ptrans_w.cpp" line="55">
        </location>
        <source>Translated path</source>
        <translation type="unfinished">Переведенный путь</translation>
    </message>
    <message>
        <location filename="../ptrans_w.cpp" line="110">
        </location>
        <source>Original path</source>
        <translation>Изначальный путь</translation>
    </message>
</context>
<context>
    <name>EditTransBase</name>
    <message>
        <location filename="../ptrans.ui" line="14">
        </location>
        <source>Path Translations</source>
        <translation>Корректировка путей</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="22">
        </location>
        <source>Setting path translations for </source>
        <translation>Задать корректировку для</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="32">
        </location>
        <source>Select one or several file types, then use the controls in the frame below to change how they are processed</source>
        <translation>Выберите типы файлов и используйте кнопки управления ниже, чтобы изменить порядок обработки файлов</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="88">
        </location>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="98">
        </location>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="122">
        </location>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../ptrans.ui" line="129">
        </location>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
</context>
<context>
    <name>FirstIdxDialog</name>
    <message>
        <location filename="../firstidx.ui" line="14">
        </location>
        <source>First indexing setup</source>
        <translation>Настройка первого индексирования</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="28">
        </location>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;It appears that the index for this configuration does not exist.&lt;/span&gt;&lt;br /&gt;&lt;br /&gt;If you just want to index your home directory with a set of reasonable defaults, press the &lt;span style=&quot; font-style:italic;&quot;&gt;Start indexing now&lt;/span&gt; button. You will be able to adjust the details later. &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you want more control, use the following links to adjust the indexing configuration and schedule.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These tools can be accessed later from the &lt;span style=&quot; font-style:italic;&quot;&gt;Preferences&lt;/span&gt; menu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Похоже, что индекс для этой конфигурации не существует.&lt;/span&gt;&lt;br /&gt;&lt;br /&gt;Для индексирования только домашнего каталога с набором умолчаний нажмите кнопку &lt;span style=&quot; font-style:italic;&quot;&gt;«Запустить индексирование»&lt;/span&gt;. Детальную настройку можно будет провести позже. &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если нужно больше контроля, воспользуйтесь приведёнными ниже ссылками для настройки параметров и расписания индексирования.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Перейти к этим инструментам позднее можно через меню &lt;span style=&quot; font-style:italic;&quot;&gt;«Настройка»&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="49">
        </location>
        <source>Indexing configuration</source>
        <translation>Настройка индексирования</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="58">
        </location>
        <source>This will let you adjust the directories you want to index, and other parameters like excluded file paths or names, default character sets, etc.</source>
        <translation>Здесь можно указать, какие каталоги требуется индексировать, а также настроить такие параметры как исключение путей или имён файлов, используемые по умолчанию кодировки и т.д.</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="65">
        </location>
        <source>Indexing schedule</source>
        <translation>Расписание индексирования</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="71">
        </location>
        <source>This will let you chose between batch and real-time indexing, and set up an automatic  schedule for batch indexing (using cron).</source>
        <translation>Здесь можно выбрать режим индексирования: по расписанию или в реальном времени, а также настроить расписание автоиндексирования (с использованием cron).</translation>
    </message>
    <message>
        <location filename="../firstidx.ui" line="78">
        </location>
        <source>Start indexing now</source>
        <translation>Запустить индексирование</translation>
    </message>
</context>
<context>
    <name>FragButs</name>
    <message>
        <location filename="../fragbuts.cpp" line="145">
        </location>
        <source>%1 not found.</source>
        <translation>%1 не найден.</translation>
    </message>
    <message>
        <location filename="../fragbuts.cpp" line="151">
        </location>
        <source>%1:
 %2</source>
        <translation>%1:
 %2</translation>
    </message>
    <message>
        <location filename="../fragbuts.cpp" line="158">
        </location>
        <source>Query Fragments</source>
        <translation>Фрагменты запроса</translation>
    </message>
</context>
<context>
    <name>IdxSchedW</name>
    <message>
        <location filename="../idxsched.ui" line="14">
        </location>
        <source>Index scheduling setup</source>
        <translation>Настройка расписания индексирования</translation>
    </message>
    <message>
        <location filename="../idxsched.ui" line="28">
        </location>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; indexing can run permanently, indexing files as they change, or run at discrete intervals. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Reading the manual may help you to decide between these approaches (press F1). &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This tool can help you set up a schedule to automate batch indexing runs, or start real time indexing when you log in (or both, which rarely makes sense). &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Индексирование &lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; может работать постоянно, индексируя изменяющиеся файлы, или запускаться через определённые промежутки времени. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Рекомендуется ознакомиться с руководством пользователя программы, чтобы выбрать наиболее подходящий режим работы (нажмите F1 для вызова справки). &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Этот инструмент позволяет выбрать, будет ли индексирование производиться по расписанию или в реальном времени при входе в систему (или оба варианта сразу, что вряд ли имеет смысл). &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../idxsched.ui" line="48">
        </location>
        <source>Cron scheduling</source>
        <translation>Расписание запуска</translation>
    </message>
    <message>
        <location filename="../idxsched.ui" line="54">
        </location>
        <source>The tool will let you decide at what time indexing should run and will install a crontab entry.</source>
        <translation>Этот инструмент позволяет выбрать, в какое время запускать индексирование, а также сделать запись в crontab.</translation>
    </message>
    <message>
        <location filename="../idxsched.ui" line="61">
        </location>
        <source>Real time indexing start up</source>
        <translation>Запуск индексирования в реальном времени</translation>
    </message>
    <message>
        <location filename="../idxsched.ui" line="67">
        </location>
        <source>Decide if real time indexing will be started when you log in (only for the default index).</source>
        <translation>Здесь можно выбрать, нужно ли начинать индексирование в реальном времени при входе в систему (только для индекса по умолчанию).</translation>
    </message>
</context>
<context>
    <name>ListDialog</name>
    <message>
        <location filename="../widgets/listdialog.ui" line="14">
        </location>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../widgets/listdialog.ui" line="20">
        </location>
        <source>GroupBox</source>
        <translation>GroupBox</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../main.cpp" line="463">
        </location>
        <source>&quot;history&quot; file is damaged, please check or remove it: </source>
        <translation>Файл истории повреждён, проверьте или удалите его: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="495">
        </location>
        <source>No db directory in configuration</source>
        <translation>Каталог базы не задан в конфигурации</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="507">
        </location>
        <source>Needs &quot;Show system tray icon&quot; to be set in preferences!
</source>
        <translation>Необходимо включить параметр настройки «Отображать значок в трее»!
</translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <location filename="../preview.ui" line="14">
        </location>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="27">
        </location>
        <source>Tab 1</source>
        <translation>1-я вкладка</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="37">
        </location>
        <source>&amp;Search for:</source>
        <translation>&amp;Искать:</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="57">
        </location>
        <source>&amp;Next</source>
        <translation>&amp;Следующий</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="64">
        </location>
        <source>&amp;Previous</source>
        <translation>&amp;Предыдущий</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="74">
        </location>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="81">
        </location>
        <source>Match &amp;Case</source>
        <translation>&amp;С учётом регистра</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="118">
        </location>
        <source>Previous result document</source>
        <translation>Предыдущий документ с результатами</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="132">
        </location>
        <source>Next result document</source>
        <translation>Следующий документ с результатами</translation>
    </message>
    <message>
        <location filename="../preview.ui" line="163">
        </location>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="188">
        </location>
        <location filename="../preview_w.cpp" line="190">
        </location>
        <location filename="../preview_w.cpp" line="192">
        </location>
        <location filename="../preview_w.cpp" line="194">
        </location>
        <location filename="../preview_w.cpp" line="196">
        </location>
        <location filename="../preview_w.cpp" line="224">
        </location>
        <location filename="../preview_w.cpp" line="226">
        </location>
        <location filename="../preview_w.cpp" line="228">
        </location>
        <location filename="../preview_w.cpp" line="230">
        </location>
        <location filename="../preview_w.cpp" line="233">
        </location>
        <source>Preview Window</source>
        <translation>Окно предпросмотра</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="189">
        </location>
        <location filename="../preview_w.cpp" line="225">
        </location>
        <source>Close preview window</source>
        <translation>Закрыть окно предпросмотра</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="190">
        </location>
        <location filename="../preview_w.cpp" line="227">
        </location>
        <source>Show next result</source>
        <translation>Показать следующий результат</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="193">
        </location>
        <location filename="../preview_w.cpp" line="229">
        </location>
        <source>Show previous result</source>
        <translation>Показать предыдущий результат</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="194">
        </location>
        <location filename="../preview_w.cpp" line="231">
        </location>
        <source>Close tab</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="197">
        </location>
        <location filename="../preview_w.cpp" line="234">
        </location>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="741">
        </location>
        <source>Error loading the document: file missing.</source>
        <translation>Ошибка загрузки документа: файл отсутствует.</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="744">
        </location>
        <source>Error loading the document: no permission.</source>
        <translation>Ошибка загрузки документа: нет разрешения.</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="748">
        </location>
        <source>Error loading: backend not configured.</source>
        <translation>Ошибка загрузки: бэкенд не настроен.</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="752">
        </location>
        <source>Error loading the document: other handler error&lt;br&gt;Maybe the application is locking the file ?</source>
        <translation>загрузки документа: ошибка другого обработчика&lt;br&gt;Может, приложение заблокировало файл?</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="755">
        </location>
        <source>Error loading the document: other handler error.</source>
        <translation>Ошибка загрузки документа: ошибка другого обработчика.</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="760">
        </location>
        <source>&lt;br&gt;Attempting to display from stored text.</source>
        <translation>&lt;br&gt;Попытка отобразить из сохранённого текста.</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="794">
        </location>
        <source>Missing helper program: </source>
        <translation>Отсутствует обработчики: </translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="795">
        </location>
        <source>Can&apos;t turn doc into internal representation for </source>
        <translation>Невозможно сконвертировать документ во внутреннее представление для </translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="799">
        </location>
        <source>Canceled</source>
        <translation>Отменено</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="853">
        </location>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="888">
        </location>
        <source>Could not fetch stored text</source>
        <translation>Не удалось получить сохранённый текст</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="937">
        </location>
        <source>Creating preview text</source>
        <translation>Создание текста для просмотра</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1000">
        </location>
        <source>Loading preview text into editor</source>
        <translation>Загрузка текста в редактор</translation>
    </message>
</context>
<context>
    <name>PreviewTextEdit</name>
    <message>
        <location filename="../preview_w.cpp" line="1160">
        </location>
        <location filename="../preview_w.cpp" line="1171">
        </location>
        <source>Show fields</source>
        <translation>Показать поля</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1162">
        </location>
        <location filename="../preview_w.cpp" line="1167">
        </location>
        <source>Show image</source>
        <translation>Показать изображение</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1165">
        </location>
        <location filename="../preview_w.cpp" line="1172">
        </location>
        <source>Show main text</source>
        <translation>Показать основной текст</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1175">
        </location>
        <source>Reload as Plain Text</source>
        <translation>Перезагрузить как простой текст</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1176">
        </location>
        <source>Reload as HTML</source>
        <translation>Перезагрузить как HTML</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1178">
        </location>
        <source>Select All</source>
        <translation>Выделить всё</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1179">
        </location>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1182">
        </location>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1185">
        </location>
        <source>Fold lines</source>
        <translation>Линия сгиба</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1187">
        </location>
        <source>Preserve indentation</source>
        <translation>Сохранить отступы</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1190">
        </location>
        <source>Save document to file</source>
        <translation>Сохранить документ в файл</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1192">
        </location>
        <source>Open document</source>
        <translation>Открыть документ</translation>
    </message>
    <message>
        <location filename="../preview_w.cpp" line="1363">
        </location>
        <source>Print Current Preview</source>
        <translation>Печать текущего вида</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="450">
        </location>
        <source>&lt;b&gt;Customised subtrees</source>
        <translation>&lt;b&gt;Пользовательские подкаталоги</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="451">
        </location>
        <source>The list of subdirectories in the indexed hierarchy &lt;br&gt;where some parameters need to be redefined. Default: empty.</source>
        <translation>Список подкаталогов индексируемого дерева,&lt;br&gt;к которым должны применяться особые параметры. По умолчанию: пусто.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="482">
        </location>
        <source>&lt;i&gt;The parameters that follow are set either at the top level, if nothing or an empty line is selected in the listbox above, or for the selected subdirectory. You can add or remove directories by clicking the +/- buttons.</source>
        <translation>&lt;i&gt;</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="504">
        </location>
        <source>Skipped names</source>
        <translation>Пропускать</translation>
    </message>
    <message>
        <source>These are patterns for file or directory  names which should not be indexed.</source>
        <translation type="vanished">Шаблоны имён файлов или каталогов, имена которых не следует индексировать.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="505">
        </location>
        <source>These are patterns for file or directory names which should not be indexed.</source>
        <translation type="unfinished">Это шаблоны для имен файлов или каталогов, которые не должны быть проиндексированы.</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="541">
        </location>
        <source>Ignored endings</source>
        <translation>Игнорируемые окончания</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="542">
        </location>
        <source>These are file name endings for files which will be indexed by name only 
(no MIME type identification attempt, no decompression, no content indexing).</source>
        <translation>Окончания имён файлов, индексируемых только по имени
(без попытки определить типы MIME, разжатия либо индексации содержимого).</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="571">
        </location>
        <source>Default&lt;br&gt;character set</source>
        <translation>Кодировка по умолчанию</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="572">
        </location>
        <source>Character set used for reading files which do not identify the character set internally, for example pure text files.&lt;br&gt;The default value is empty, and the value from the NLS environnement is used.</source>
        <translation>Кодировка, которая будет использована при чтении файлов, в которых кодировка не указана явно; например, простых текстовых файлов.&lt;br&gt;Значение по умолчанию не установлено и берётся из параметров системы (локали).</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="585">
        </location>
        <source>Follow symbolic links</source>
        <translation>Открывать символические ссылки</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="586">
        </location>
        <source>Follow symbolic links while indexing. The default is no, to avoid duplicate indexing</source>
        <translation>Открывать символические ссылки при индексировании. По умолчанию действие не выполняется во избежание дублированного индексирования</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="596">
        </location>
        <source>Index all file names</source>
        <translation>Индексировать все имена файлов</translation>
    </message>
    <message>
        <location filename="../confgui/confguiindex.cpp" line="597">
        </location>
        <source>Index the names of files for which the contents cannot be identified or processed (no or unsupported mime type). Default true</source>
        <translation>Индексировать имена файлов, содержимое которых невозможно определить или обработать (неизвестный или неподдерживаемый тип MIME). По умолчанию включено</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../multisave.cpp" line="42">
        </location>
        <source>Create or choose save directory</source>
        <translation>Создать или выбрать каталог сохранения</translation>
    </message>
    <message>
        <location filename="../multisave.cpp" line="52">
        </location>
        <source>Choose exactly one directory</source>
        <translation>Выберите только один каталог</translation>
    </message>
    <message>
        <location filename="../multisave.cpp" line="83">
        </location>
        <source>Could not read directory: </source>
        <translation>Невозможно прочитать каталог:</translation>
    </message>
    <message>
        <location filename="../multisave.cpp" line="128">
        </location>
        <source>Unexpected file name collision, cancelling.</source>
        <translation>Неожиданный конфликт имён файлов, отмена действия.</translation>
    </message>
    <message>
        <location filename="../multisave.cpp" line="136">
        </location>
        <source>Cannot extract document: </source>
        <translation>Невозможно извлечь документ: </translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="58">
        </location>
        <source>&amp;Preview</source>
        <translation>&amp;Просмотр</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="61">
        </location>
        <source>&amp;Open</source>
        <translation>О&amp;ткрыть</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="69">
        </location>
        <source>Open With</source>
        <translation>Открыть с помощью</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="86">
        </location>
        <source>Run Script</source>
        <translation>Запустить выполнение сценария</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="101">
        </location>
        <source>Copy &amp;File Path</source>
        <translation>Копировать &amp;путь файла</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="103">
        </location>
        <source>Copy &amp;URL</source>
        <translation>Копировать &amp;URL</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="104">
        </location>
        <source>Copy File Name</source>
        <translation>Копировать &amp;имя файла</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="105">
        </location>
        <source>Copy Text</source>
        <translation>Копировать текст</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="108">
        </location>
        <source>&amp;Write to File</source>
        <translation>&amp;Записать в файл</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="111">
        </location>
        <source>Save selection to files</source>
        <translation>Сохранить выделение в файлы</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="120">
        </location>
        <source>Preview P&amp;arent document/folder</source>
        <translation>&amp;Просмотр родительского документа/каталога</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="122">
        </location>
        <source>&amp;Open Parent document</source>
        <translation>&amp;Открыть родительский документ</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="125">
        </location>
        <source>&amp;Open Parent Folder</source>
        <translation>&amp;Открыть родительский каталог</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="128">
        </location>
        <source>Find &amp;similar documents</source>
        <translation>Найти &amp;похожие документы</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="131">
        </location>
        <source>Open &amp;Snippets window</source>
        <translation>Открыть окно &amp;выдержек</translation>
    </message>
    <message>
        <location filename="../respopup.cpp" line="134">
        </location>
        <source>Show subdocuments / attachments</source>
        <translation>Показать вложенные документы</translation>
    </message>
</context>
<context>
    <name>QxtConfirmationMessage</name>
    <message>
        <location filename="../widgets/qxtconfirmationmessage.cpp" line="64">
        </location>
        <source>Do not show again.</source>
        <translation>Больше не показывать.</translation>
    </message>
</context>
<context>
    <name>RTIToolW</name>
    <message>
        <location filename="../rtitool.ui" line="14">
        </location>
        <source>Real time indexing automatic start</source>
        <translation>Автозапуск индексирования в реальном времени</translation>
    </message>
    <message>
        <location filename="../rtitool.ui" line="22">
        </location>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; indexing can be set up to run as a daemon, updating the index as files change, in real time. You gain an always up to date index, but system resources are used permanently.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Индексирование при помощи &lt;span style=&quot; font-weight:600;&quot;&gt;Recoll&lt;/span&gt; может быть настроено как сервис, обновляющий индекс одновременно с изменением файлов, то есть в реальном времени. При этом постоянное обновление индекса будет происходить за счёт непрерывного использования системных ресурсов.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../rtitool.ui" line="37">
        </location>
        <source>Start indexing daemon with my desktop session.</source>
        <translation>Запускать службу индексирования одновременно с сеансом рабочего стола.</translation>
    </message>
    <message>
        <location filename="../rtitool.ui" line="68">
        </location>
        <source>Also start indexing daemon right now.</source>
        <translation>Также запустить прямо сейчас службу индексирования.</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="88">
        </location>
        <source>Replacing: </source>
        <translation>Замена: </translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="91">
        </location>
        <source>Replacing file</source>
        <translation>Замена файла</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="118">
        </location>
        <source>Can&apos;t create: </source>
        <translation>Невозможно создать: </translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="119">
        </location>
        <location filename="../rtitool.cpp" line="133">
        </location>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="134">
        </location>
        <source>Could not execute recollindex</source>
        <translation>Не удалось запустить recollindex</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="144">
        </location>
        <source>Deleting: </source>
        <translation>Удаление: </translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="147">
        </location>
        <source>Deleting file</source>
        <translation>Удаление файла</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="158">
        </location>
        <source>Removing autostart</source>
        <translation>Отмена автозапуска</translation>
    </message>
    <message>
        <location filename="../rtitool.cpp" line="159">
        </location>
        <source>Autostart file deleted. Kill current process too ?</source>
        <translation>Файл автозапуска удалён. Прервать текущий процесс?</translation>
    </message>
</context>
<context>
    <name>RclCompleterModel</name>
    <message>
        <location filename="../ssearch_w.cpp" line="145">
        </location>
        <source>Hits</source>
        <translation>(количество нажатий)</translation>
    </message>
</context>
<context>
    <name>RclMain</name>
    <message>
        <location filename="../rclm_idx.cpp" line="53">
        </location>
        <source>Indexing in progress: </source>
        <translation>Идёт индексирование:</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="56">
        </location>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="57">
        </location>
        <source>Updating</source>
        <translation>Обновление</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="58">
        </location>
        <source>Flushing</source>
        <translation>Заполнение</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="59">
        </location>
        <source>Purge</source>
        <translation>Очистка</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="60">
        </location>
        <source>Stemdb</source>
        <translation>Корнебаза</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="61">
        </location>
        <source>Closing</source>
        <translation>Закрытие</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="62">
        </location>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="63">
        </location>
        <source>Monitor</source>
        <translation>Монитор</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="64">
        </location>
        <source>Unknown</source>
        <translation>неизвестно</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="68">
        </location>
        <source>documents</source>
        <translation>документы</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="68">
        </location>
        <source>document</source>
        <translation>документ</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="69">
        </location>
        <source>files</source>
        <translation>файлы</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="69">
        </location>
        <source>file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="70">
        </location>
        <source>errors</source>
        <translation>ошибки</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="70">
        </location>
        <source>error</source>
        <translation>ошибка</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="74">
        </location>
        <source>total files)</source>
        <translation>всего файлов)</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="149">
        </location>
        <source>Indexing interrupted</source>
        <translation>Индексирование прервано</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="152">
        </location>
        <source>Indexing failed</source>
        <translation>Не удалось выполнить индексирование</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="154">
        </location>
        <source>with additional message: </source>
        <translation>с дополнительным сообщением: </translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="164">
        </location>
        <source>Non-fatal indexing message: </source>
        <translation>Сообщение о некритичной ошибке индексирования: </translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="184">
        </location>
        <location filename="../rclm_idx.cpp" line="217">
        </location>
        <source>Stop &amp;Indexing</source>
        <translation>О&amp;становить индексирование</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="198">
        </location>
        <source>Index locked</source>
        <translation>Индекс заблокирован</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="207">
        </location>
        <source>Update &amp;Index</source>
        <translation>Обновить &amp;индекс</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="235">
        </location>
        <source>Indexing done</source>
        <translation>Индексация завершена</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="257">
        </location>
        <source>Bad paths</source>
        <translation>Неверные пути</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="258">
        </location>
        <source>Empty or non-existant paths in configuration file. Click Ok to start indexing anyway (absent data will not be purged from the index):
</source>
        <translation>Пустые или несуществующие пути в файле конфигурации. Нажмите Ok, если всё равно хотите запустить процесс индексирования (отсутствующие данные не будут убраны из файла индекса):
</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="387">
        </location>
        <source>Erasing index</source>
        <translation>Стирание индекса</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="387">
        </location>
        <source>Reset the index and start from scratch ?</source>
        <translation>Сбросить индекс и начать заново?</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="554">
        </location>
        <source>Selection patterns need topdir</source>
        <translation>Для шаблонов отбора требуется topdir</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="555">
        </location>
        <source>Selection patterns can only be used with a start directory</source>
        <translation>Шаблоны отбора могут быть использованы только с начальным каталогом</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="577">
        </location>
        <location filename="../rclm_idx.cpp" line="595">
        </location>
        <location filename="../rclm_preview.cpp" line="126">
        </location>
        <location filename="../rclm_preview.cpp" line="204">
        </location>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="577">
        </location>
        <source>Can&apos;t update index: indexer running</source>
        <translation>Невозможно обновить индекс: индексатор уже запущен</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="595">
        </location>
        <source>Can&apos;t update index: internal error</source>
        <translation>Невозможно обновить индекс: внутренняя ошибка</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="107">
        </location>
        <source>Simple search type</source>
        <translation>Тип простого поиска</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="109">
        </location>
        <source>Any term</source>
        <translation>Любое слово</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="115">
        </location>
        <source>All terms</source>
        <translation>Все слова</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="120">
        </location>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="125">
        </location>
        <source>Query language</source>
        <translation>Язык запроса</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="137">
        </location>
        <source>Stemming language</source>
        <translation>Язык словоформ</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="139">
        </location>
        <source>(no stemming)</source>
        <translation>(без словоформ)</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="140">
        </location>
        <source>(all languages)</source>
        <translation>(все языки)</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="154">
        </location>
        <source>error retrieving stemming languages</source>
        <translation>ошибка получения списка языков словоформ</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="75">
        </location>
        <location filename="../rclm_view.cpp" line="453">
        </location>
        <source>Can&apos;t access file: </source>
        <translation>Невозможно получить доступ к файлу: </translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="98">
        </location>
        <source>Index not up to date for this file.&lt;br&gt;</source>
        <translation>Индекс для этого файла не обновлён.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="100">
        </location>
        <source>&lt;em&gt;Also, it seems that the last index update for the file failed.&lt;/em&gt;&lt;br/&gt;</source>
        <translation>&lt;em&gt;Кстати, похоже, последняя попытка обновления для этого файла также не удалась.&lt;/em&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="104">
        </location>
        <source>Click Ok to try to update the index for this file. You will need to run the query again when indexing is done.&lt;br&gt;</source>
        <translation>Щёлкните по кнопке Ok, чтобы попытаться обновить индекс для этого файла. По окончании индексирования нужно будет повторить запрос.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="108">
        </location>
        <source>The indexer is running so things should improve when it&apos;s done. </source>
        <translation>Индексация выполняется, по завершении должно стать лучше.</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="112">
        </location>
        <source>The document belongs to an external index which I can&apos;t update. </source>
        <translation>Документ относится к внешнему индексу, который невозможно обновить.</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="115">
        </location>
        <source>Click Cancel to return to the list.&lt;br&gt;Click Ignore to show the preview anyway (and remember for this session). There is a risk of showing the wrong entry.&lt;br/&gt;</source>
        <translation>&gt;Нажмите «Отмена» для возврата к списку. &lt;br&gt;Нажмите «Игнорировать», чтобы открыть просмотр (и запомнить выбор для данного сеанса).</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="205">
        </location>
        <source>Can&apos;t create preview window</source>
        <translation>Невозможно создать окно просмотра</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="242">
        </location>
        <source>This search is not active anymore</source>
        <translation>Этот поиск больше не активен</translation>
    </message>
    <message>
        <source>This search is not active any more</source>
        <translation type="vanished">Этот поиск больше не активен</translation>
    </message>
    <message>
        <location filename="../rclm_preview.cpp" line="262">
        </location>
        <source>Cannot retrieve document info from database</source>
        <translation>Невозможно извлечь сведения о документе из базы</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="66">
        </location>
        <source>No search</source>
        <translation>Результаты поиска отсутствуют</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="66">
        </location>
        <source>No preserved previous search</source>
        <translation>Отсутствуют сохранённые результаты предыдущего поиска</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="72">
        </location>
        <source>Choose file to save</source>
        <translation>Выбор файла для сохранения</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="73">
        </location>
        <location filename="../rclm_saveload.cpp" line="107">
        </location>
        <source>Saved Queries (*.rclq)</source>
        <translation>Сохраненные запросы (*.rclq)</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="98">
        </location>
        <source>Write failed</source>
        <translation>Не удалось записать</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="98">
        </location>
        <source>Could not write to file</source>
        <translation>Не удалось выполнить запись в файл</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="114">
        </location>
        <source>Read failed</source>
        <translation>Ошибка записи</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="114">
        </location>
        <source>Could not open file: </source>
        <translation>Не удалось открыть файл: </translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="134">
        </location>
        <source>Load error</source>
        <translation>Ошибка загрузки</translation>
    </message>
    <message>
        <location filename="../rclm_saveload.cpp" line="134">
        </location>
        <source>Could not load saved query</source>
        <translation>Не удалось загрузить сохранённый запрос</translation>
    </message>
    <message>
        <location filename="../rclm_sidefilters.cpp" line="42">
        </location>
        <source>Filter directories</source>
        <translation>Фильтровать каталоги</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="129">
        </location>
        <source>Bad desktop app spec for %1: [%2]
Please check the desktop file</source>
        <translation>Неверная спецификация для %1: [%2]
Проверьте файл .desktop</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="282">
        </location>
        <source>No external viewer configured for mime type [</source>
        <translation>Не настроена внешняя программа для просмотра MIME-типа [</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="305">
        </location>
        <source>Bad viewer command line for %1: [%2]
Please check the mimeview file</source>
        <translation>Ошибка командной строки программы просмотра %1: [%2]
Проверьте файл mimeview</translation>
    </message>
    <message>
        <source>The viewer specified in mimeview for %1: %2 is not found.
Do you want to start the  preferences dialog ?</source>
        <translation type="vanished">Программа просмотра, указанная в mimeview для %1: %2, не найдена.
Открыть диалог настройки?</translation>
    </message>
    <message>
        <source>Viewer command line for %1 specifies parent file but URL is http[s]: unsupported</source>
        <translation type="vanished">В командной строке программы просмотра %1 указан родительский файл, а в URL — сетевой протокол http[s]: не поддерживается</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="339">
        </location>
        <source>The viewer specified in mimeview for %1: %2 is not found.
Do you want to start the preferences dialog ?</source>
        <translation type="unfinished">Просмотрщик, указанный в mimeview для %1: %2, не найден. Хотите открыть диалоговое окно настроек?</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="384">
        </location>
        <source>Viewer command line for %1 specifies parent file but URL is not file:// : unsupported</source>
        <translation>В командной строке программы просмотра %1 указан родительский файл, а в URL — не file:// : не поддерживается</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="390">
        </location>
        <source>Viewer command line for %1 specifies both file and parent file value: unsupported</source>
        <translation>В командной строке программы просмотра %1 указан как сам файл, так и родительский файл: не поддерживается</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="404">
        </location>
        <source>Cannot find parent document</source>
        <translation>Невозможно найти родительский документ</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="439">
        </location>
        <location filename="../rclmain_w.cpp" line="1042">
        </location>
        <source>Cannot extract document or create temporary file</source>
        <translation>Невозможно извлечь документ или создать временный файл</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="459">
        </location>
        <source>Can&apos;t uncompress file: </source>
        <translation>Невозможно распаковать файл: </translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="474">
        </location>
        <source>Opening a temporary copy. Edits will be lost if you don&apos;t save&lt;br/&gt;them to a permanent location.</source>
        <translation>Открывается временная копия. Изменения будут утеряны, если их не сохранить&lt;br/&gt;особо.</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="476">
        </location>
        <source>Do not show this warning next time (use GUI preferences to restore).</source>
        <translation>Больше не показывать (для восстановления используйте настройки интерфейса).</translation>
    </message>
    <message>
        <location filename="../rclm_view.cpp" line="568">
        </location>
        <source>Executing: [</source>
        <translation>Выполняется: [</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="91">
        </location>
        <source>Unknown indexer state. Can&apos;t access webcache file.</source>
        <translation>Неизвестный статус индексатора. Невозможно получить доступ к файлу веб-кэша.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="96">
        </location>
        <source>Indexer is running. Can&apos;t access webcache file.</source>
        <translation>Идёт индексирование. Невозможно получить доступ к файлу веб-кэша.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="215">
        </location>
        <source>Batch scheduling</source>
        <translation>Планирование</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="217">
        </location>
        <source>The tool will let you decide at what time indexing should run.  It uses the Windows task scheduler.</source>
        <translation>Инструмент для настройки времени запуска индексации. Использует планировщик задач Windows.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="230">
        </location>
        <source>Disabled because the real time indexer was not compiled in.</source>
        <translation>Отключено, так как не был вкомпилирован индексатор данных в реальном времени.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="234">
        </location>
        <source>This configuration tool only works for the main index.</source>
        <translation>Данный инструмент настройки применим только к основному индексу.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="344">
        </location>
        <source>About Recoll</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="352">
        </location>
        <source>No information: initial indexing not yet performed.</source>
        <translation>Нет данных: первичная индексация ещё не проведена.</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="356">
        </location>
        <source>External applications/commands needed for your file types and not found, as stored by the last indexing pass in </source>
        <translation>Внешние приложения/команды, требуемые для индексирования файлов, не найдены, как указано в результатах последнего индексирования в </translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="364">
        </location>
        <source>No helpers found missing</source>
        <translation>Все обработчики доступны</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="367">
        </location>
        <source>Missing helper programs</source>
        <translation>Отсутствующие обработчики</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="375">
        </location>
        <location filename="../rclm_wins.cpp" line="385">
        </location>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="386">
        </location>
        <source>Index query error</source>
        <translation>Ошибка запроса индекса</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="424">
        </location>
        <source>Indexed MIME Types</source>
        <translation>Проиндексированные MIME-типы</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="427">
        </location>
        <source>Content has been indexed for these MIME types:</source>
        <translation>Было проиндексировано содержимое для следующих типов MIME:</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="436">
        </location>
        <source>Types list empty: maybe wait for indexing to progress?</source>
        <translation>Список типов пуст: может, обождать доиндексирования?</translation>
    </message>
    <message>
        <location filename="../rclm_wins.cpp" line="455">
        </location>
        <location filename="../rclm_wins.cpp" line="456">
        </location>
        <source>Duplicates</source>
        <translation>Дубликаты</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="351">
        </location>
        <location filename="../rclmain_w.cpp" line="353">
        </location>
        <location filename="../rclmain_w.cpp" line="355">
        </location>
        <location filename="../rclmain_w.cpp" line="359">
        </location>
        <location filename="../rclmain_w.cpp" line="361">
        </location>
        <location filename="../rclmain_w.cpp" line="363">
        </location>
        <source>Main Window</source>
        <translation>Главное окно</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="351">
        </location>
        <source>Clear search</source>
        <translation>Очистить поиск</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="353">
        </location>
        <source>Move keyboard focus to search entry</source>
        <translation>Перенести фокус ввода на строку поиска</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="355">
        </location>
        <source>Move keyboard focus to search, alt.</source>
        <translation>Перенести фокус ввода на строку поиска, также</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="359">
        </location>
        <source>Toggle tabular display</source>
        <translation>Переключить табличное отображение</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="361">
        </location>
        <source>Show menu search dialog</source>
        <translation>Показать диалоговое окно поиска</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="363">
        </location>
        <source>Move keyboard focus to table</source>
        <translation>Перенести фокус клавиатуры на таблицу</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="378">
        </location>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="395">
        </location>
        <source>Results</source>
        <translation>Результаты</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="449">
        </location>
        <location filename="../rclmain_w.cpp" line="458">
        </location>
        <location filename="../rclmain_w.cpp" line="474">
        </location>
        <location filename="../rclmain_w.cpp" line="489">
        </location>
        <source>All</source>
        <translation>Всё</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="449">
        </location>
        <source>media</source>
        <translation>медиа</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="449">
        </location>
        <source>message</source>
        <translation>сообщение</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="450">
        </location>
        <source>other</source>
        <translation>прочее</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="450">
        </location>
        <source>presentation</source>
        <translation>презентация</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="451">
        </location>
        <source>spreadsheet</source>
        <translation>таблица</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="451">
        </location>
        <source>text</source>
        <translation>текст</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="452">
        </location>
        <location filename="../rclmain_w.cpp" line="957">
        </location>
        <source>sorted</source>
        <translation>сортированное</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="452">
        </location>
        <location filename="../rclmain_w.cpp" line="959">
        </location>
        <source>filtered</source>
        <translation>отфильтрованное</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="459">
        </location>
        <source>Document filter</source>
        <translation>Фильтр документов</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="486">
        </location>
        <source>F&amp;ilter</source>
        <translation>Ф&amp;ильтр</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="606">
        </location>
        <source>Main index open error: </source>
        <translation>Ошибка открытия основного индекса: </translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="607">
        </location>
        <source>. The index may be corrupted. Maybe try to run xapian-check or rebuild the index ?.</source>
        <translation>Индекс может быть повреждён. Попробуйте запустить xapian-check или пересоздать индекс?</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="611">
        </location>
        <source>Could not open external index. Db not open. Check external indexes list.</source>
        <translation>Не удалось открыть внешний индекс. База не открыта. Проверьте список внешних индексов.</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="785">
        </location>
        <source>Can&apos;t set synonyms file (parse error?)</source>
        <translation>Невозможно установить файл синонимов (ошибка разбора?)</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="797">
        </location>
        <source>Query results</source>
        <translation>Результаты запроса</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="842">
        </location>
        <source>Query in progress.&lt;br&gt;Due to limitations of the indexing library,&lt;br&gt;cancelling will exit the program</source>
        <translation>Идёт обработка запроса.&lt;br&gt;Из-за ограничений библиотеки&lt;br&gt;отмена действия приведёт к закрытию приложения</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="874">
        </location>
        <source>Result count (est.)</source>
        <translation>Кол-во результатов (прим.)</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="876">
        </location>
        <source>No results found</source>
        <translation>Поиск не дал результатов</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1038">
        </location>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1062">
        </location>
        <location filename="../rclmain_w.cpp" line="1063">
        </location>
        <source>Sub-documents and attachments</source>
        <translation>Вложенные документы</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1114">
        </location>
        <location filename="../rclmain_w.cpp" line="1120">
        </location>
        <source>History data</source>
        <translation>Данные истории</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1119">
        </location>
        <source>Document history</source>
        <translation>История документов</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1145">
        </location>
        <source>Confirm</source>
        <translation>Подтвердить</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1146">
        </location>
        <source>Erasing simple and advanced search history lists, please click Ok to confirm</source>
        <translation>Стираю историю простого и сложного поиска, нажмите Ok для подтверждения</translation>
    </message>
    <message>
        <location filename="../rclmain_w.cpp" line="1172">
        </location>
        <source>Could not open/create file</source>
        <translation>Не удалось открыть/создать файл</translation>
    </message>
</context>
<context>
    <name>RclMainBase</name>
    <message>
        <location filename="../rclmain.ui" line="20">
        </location>
        <source>Recoll</source>
        <translation>Recoll</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="49">
        </location>
        <source>Query Language Filters</source>
        <translation>Фильтры языка запросов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="63">
        </location>
        <source>Filter dates</source>
        <translation>Фильтровать по дате</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="91">
        </location>
        <source>Filter birth dates</source>
        <translation>Фильтровать по дате рождения</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="159">
        </location>
        <source>E&amp;xit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="162">
        </location>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="170">
        </location>
        <source>Update &amp;index</source>
        <translation>Обновить индекс</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="181">
        </location>
        <source>Trigger incremental pass</source>
        <translation>Запустить пошаговый проход</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="192">
        </location>
        <source>Start real time indexer</source>
        <translation>Запустить индексацию в фоне</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="200">
        </location>
        <source>&amp;Rebuild index</source>
        <translation>Пересоздать индекс</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="208">
        </location>
        <source>&amp;Erase document history</source>
        <translation>&amp;Стереть историю документов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="216">
        </location>
        <source>&amp;Erase search history</source>
        <translation>&amp;Стереть историю поиска</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="224">
        </location>
        <source>E&amp;xport simple search history</source>
        <translation>Э&amp;кспортировать историю простого поиска</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="232">
        </location>
        <source>Missing &amp;helpers</source>
        <translation>Недостающие &amp;обработчики</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="240">
        </location>
        <source>Indexed &amp;MIME types</source>
        <translation>Индексированные &amp;MIME типы</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="248">
        </location>
        <source>&amp;About Recoll</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>&amp;User manual</source>
        <translation type="vanished">&amp;Руководство пользователя</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="256">
        </location>
        <source>&amp;User manual (local, one HTML page)</source>
        <translation type="unfinished">Руководство пользователя (локальное, одна HTML-страница)</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="264">
        </location>
        <source>&amp;Online manual (Recoll Web site)</source>
        <translation type="unfinished">Онлайн-руководство (веб-сайт Recoll)</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="276">
        </location>
        <source>Document &amp;History</source>
        <translation>История &amp;документов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="279">
        </location>
        <source>Document  History</source>
        <translation>История документов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="291">
        </location>
        <source>&amp;Advanced Search</source>
        <translation>Сложный поиск</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="294">
        </location>
        <source>Assisted complex search</source>
        <translation>Сложный поиск с поддержкой</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="302">
        </location>
        <source>&amp;Sort parameters</source>
        <translation>&amp;Параметры сортировки</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="305">
        </location>
        <source>Sort parameters</source>
        <translation>Параметры сортировки</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="317">
        </location>
        <source>Term &amp;explorer</source>
        <translation>Обозреватель &amp;терминов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="320">
        </location>
        <source>Term explorer tool</source>
        <translation>Инструмент обзора терминов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="335">
        </location>
        <source>Next page</source>
        <translation>Следующая страница</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="338">
        </location>
        <source>Next page of results</source>
        <translation>Следующая страница результатов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="341">
        </location>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="356">
        </location>
        <source>First page</source>
        <translation>Первая страница</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="359">
        </location>
        <source>Go to first page of results</source>
        <translation>Перейти к первой странице результатов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="362">
        </location>
        <source>Shift+PgUp</source>
        <translation>Shift+PgUp</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="377">
        </location>
        <source>Previous page</source>
        <translation>Предыдущая страница</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="380">
        </location>
        <source>Previous page of results</source>
        <translation>Предыдущая страница результатов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="383">
        </location>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="391">
        </location>
        <source>&amp;Index configuration</source>
        <translation>Настройка &amp;индекса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="399">
        </location>
        <source>Indexing &amp;schedule</source>
        <translation>&amp;Расписание индексирования</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="407">
        </location>
        <source>&amp;GUI configuration</source>
        <translation>Настройка и&amp;нтерфейса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="415">
        </location>
        <source>E&amp;xternal index dialog</source>
        <translation>Настройка &amp;внешнего индекса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="418">
        </location>
        <source>External index dialog</source>
        <translation>Настройка внешнего индекса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="429">
        </location>
        <location filename="../rclmain.ui" line="432">
        </location>
        <source>Enable synonyms</source>
        <translation>Учитывать синонимы</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="440">
        </location>
        <source>&amp;Full Screen</source>
        <translation>Во весь &amp;экран</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="443">
        </location>
        <source>Full Screen</source>
        <translation>Во весь &amp;экран</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="446">
        </location>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="454">
        </location>
        <source>Increase results text font size</source>
        <translation>Увеличить размер шрифта результатов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="457">
        </location>
        <source>Increase Font Size</source>
        <translation>Увеличить размер шрифта</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="465">
        </location>
        <source>Decrease results text font size</source>
        <translation>Уменьшить размер шрифта результатов</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="468">
        </location>
        <source>Decrease Font Size</source>
        <translation>Уменьшить размер шрифта</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="486">
        </location>
        <source>Sort by date, oldest first</source>
        <translation>Сортировать по дате, старые вначале</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="489">
        </location>
        <source>Sort by dates from oldest to newest</source>
        <translation>Сортировать по дате от старым к новым</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="504">
        </location>
        <source>Sort by date, newest first</source>
        <translation>Сортировать по дате, новые вначале</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="507">
        </location>
        <source>Sort by dates from newest to oldest</source>
        <translation>Сортировать по дате от новых к старым</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="512">
        </location>
        <source>Show Query Details</source>
        <translation>Показать сведения о запросе</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="524">
        </location>
        <source>Show as table</source>
        <translation>Показать в виде таблицы</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="527">
        </location>
        <source>Show results in a spreadsheet-like table</source>
        <translation>Показать результаты в виде таблицы</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="532">
        </location>
        <source>Save as CSV (spreadsheet) file</source>
        <translation>Сохранить как CSV-файл</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="535">
        </location>
        <source>Saves the result into a file which you can load in a spreadsheet</source>
        <translation>Сохранить результат в файл, который можно загрузить в электронную таблицу</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="540">
        </location>
        <source>Next Page</source>
        <translation>Следующая страницы</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="545">
        </location>
        <source>Previous Page</source>
        <translation>Предыдущая страница</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="550">
        </location>
        <source>First Page</source>
        <translation>Первая страница</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="559">
        </location>
        <source>Query Fragments</source>
        <translation>Фрагменты запроса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="567">
        </location>
        <source>With failed files retrying</source>
        <translation>С повторной обработкой файлов с ошибками</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="570">
        </location>
        <source>Next update will retry previously failed files</source>
        <translation>При следующем обновлении будут повторно обработаны файлы с ошибками</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="578">
        </location>
        <source>Save last query</source>
        <translation>Сохранить последний запрос</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="583">
        </location>
        <source>Load saved query</source>
        <translation>Загрузить последний запрос</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="588">
        </location>
        <source>Special Indexing</source>
        <translation>Специальное индексирование</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="591">
        </location>
        <source>Indexing with special options</source>
        <translation>Индексирование с особыми параметрами</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="596">
        </location>
        <source>Switch Configuration...</source>
        <translation type="unfinished">Конфигурация переключателя...</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="599">
        </location>
        <source>Choose another configuration to run on, replacing this process</source>
        <translation type="unfinished">Выберите другую конфигурацию для запуска, заменяя этот процесс.</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="604">
        </location>
        <source>Index &amp;statistics</source>
        <translation>&amp;Статистика индекса</translation>
    </message>
    <message>
        <location filename="../rclmain.ui" line="609">
        </location>
        <source>Webcache Editor</source>
        <translation>Редактор веб-кэша</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="37">
        </location>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="40">
        </location>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="43">
        </location>
        <source>&amp;Tools</source>
        <translation>&amp;Инструменты</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="46">
        </location>
        <source>&amp;Preferences</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="49">
        </location>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="52">
        </location>
        <source>&amp;Results</source>
        <translation>&amp;Результаты</translation>
    </message>
    <message>
        <location filename="../rclm_menus.cpp" line="55">
        </location>
        <source>&amp;Query</source>
        <translation>&amp;Запрос</translation>
    </message>
</context>
<context>
    <name>RclTrayIcon</name>
    <message>
        <location filename="../systray.cpp" line="26">
        </location>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="../systray.cpp" line="27">
        </location>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
</context>
<context>
    <name>RecollModel</name>
    <message>
        <location filename="../restable.cpp" line="256">
        </location>
        <source>Abstract</source>
        <translation>Содержимое</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="257">
        </location>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="258">
        </location>
        <source>Document size</source>
        <translation>Размер документа</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="259">
        </location>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="260">
        </location>
        <source>File size</source>
        <translation>Размер файла</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="261">
        </location>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="262">
        </location>
        <source>File date</source>
        <translation>Дата файла</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="263">
        </location>
        <source>Ipath</source>
        <translation>I-путь</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="264">
        </location>
        <source>Keywords</source>
        <translation>Ключевые слова</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="265">
        </location>
        <source>MIME type</source>
        <translation>MIME-типы</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="266">
        </location>
        <source>Original character set</source>
        <translation>Исходная кодировка</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="267">
        </location>
        <source>Relevancy rating</source>
        <translation>Соответствие</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="268">
        </location>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="269">
        </location>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="270">
        </location>
        <location filename="../restable.cpp" line="271">
        </location>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="272">
        </location>
        <source>Date and time</source>
        <translation>Дата и время</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="489">
        </location>
        <source>Can&apos;t sort by inverse relevance</source>
        <translation>Нельзя отсортировать в обратном соответствии</translation>
    </message>
</context>
<context>
    <name>ResList</name>
    <message>
        <location filename="../reslist.cpp" line="345">
        </location>
        <source>&lt;p&gt;&lt;b&gt;No results found&lt;/b&gt;&lt;br&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Поиск не дал результатов&lt;/b&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="346">
        </location>
        <source>Documents</source>
        <translation>Документы</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="347">
        </location>
        <source>out of at least</source>
        <translation>из по меньшей мере</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="348">
        </location>
        <source>for</source>
        <translation>для</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="349">
        </location>
        <source>Previous</source>
        <translation>Предыдущий</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="350">
        </location>
        <source>Next</source>
        <translation>Следующий</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="351">
        </location>
        <source>Unavailable document</source>
        <translation>Документ недоступен</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="352">
        </location>
        <source>Preview</source>
        <translation>Просмотр</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="353">
        </location>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="354">
        </location>
        <source>Snippets</source>
        <translation>Выдержки</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="355">
        </location>
        <source>(show query)</source>
        <translation>(показать запрос)</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="356">
        </location>
        <source>&lt;p&gt;&lt;i&gt;Alternate spellings (accents suppressed): &lt;/i&gt;</source>
        <translation>&lt;p&gt;&lt;i&gt;Варианты написания (без диакритических знаков): &lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="357">
        </location>
        <source>&lt;p&gt;&lt;i&gt;Alternate spellings: &lt;/i&gt;</source>
        <translation>&lt;p&gt;&lt;i&gt;Варианты написания: &lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="358">
        </location>
        <source>This spelling guess was added to the search:</source>
        <translation>Это совпадение произношения было добавлено в поиск:</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="359">
        </location>
        <source>These spelling guesses were added to the search:</source>
        <translation>Эти совпадения произношений были добавлены в поиск:</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="528">
        </location>
        <source>Document history</source>
        <translation>История</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="536">
        </location>
        <source>Result list</source>
        <translation>Список результатов</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="1000">
        </location>
        <source>Result count (est.)</source>
        <translation>Кол-во результатов (прим.)</translation>
    </message>
    <message>
        <location filename="../reslist.cpp" line="1002">
        </location>
        <location filename="../reslist.cpp" line="1003">
        </location>
        <source>Query details</source>
        <translation>Подробности запроса</translation>
    </message>
</context>
<context>
    <name>ResTable</name>
    <message>
        <location filename="../restable.cpp" line="115">
        </location>
        <source>Use Shift+click to display the text instead.</source>
        <translation>Использовать Shift+щелчок мыши, чтобы показать текст.</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="757">
        </location>
        <location filename="../restable.cpp" line="759">
        </location>
        <location filename="../restable.cpp" line="762">
        </location>
        <location filename="../restable.cpp" line="764">
        </location>
        <location filename="../restable.cpp" line="766">
        </location>
        <location filename="../restable.cpp" line="768">
        </location>
        <location filename="../restable.cpp" line="771">
        </location>
        <location filename="../restable.cpp" line="774">
        </location>
        <source>Result Table</source>
        <translation>Таблица результатов</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="758">
        </location>
        <source>Open current result document</source>
        <translation>Открыть документ с текущими результатами</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="760">
        </location>
        <source>Open current result and quit</source>
        <translation>Открыть текущие результаты и выйти</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="762">
        </location>
        <source>Preview</source>
        <translation>Просмотр</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="764">
        </location>
        <source>Show snippets</source>
        <translation>Показать выдержки</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="766">
        </location>
        <source>Show header</source>
        <translation>Показать заголовок</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="769">
        </location>
        <source>Show vertical header</source>
        <translation>Показать вертикальный заголовок</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="772">
        </location>
        <source>Copy current result text to clipboard</source>
        <translation>Сохранить текст текущих результатов в буфер обмена</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="775">
        </location>
        <source>Copy result text and quit</source>
        <translation>Скопировать текст результатов и выйти</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1056">
        </location>
        <source>Save table to CSV file</source>
        <translation>Сохранить таблицу в CSV-файл</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1063">
        </location>
        <source>Can&apos;t open/create file: </source>
        <translation>Невозможно открыть/создать файл: </translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1370">
        </location>
        <source>%1 bytes copied to clipboard</source>
        <translation>%1 байт скопировано в буфер обмена</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1434">
        </location>
        <source>&amp;Reset sort</source>
        <translation>&amp;Сбросить сортировку</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1437">
        </location>
        <source>&amp;Save as CSV</source>
        <translation>&amp;Сохранить как CSV</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1440">
        </location>
        <source>&amp;Delete column</source>
        <translation>Удалить столбец</translation>
    </message>
    <message>
        <location filename="../restable.cpp" line="1447">
        </location>
        <source>Add &quot;%1&quot; column</source>
        <translation>Добавить столбец &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>SSearch</name>
    <message>
        <location filename="../ssearch_w.cpp" line="209">
        </location>
        <source>Any term</source>
        <translation>Любое слово</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="210">
        </location>
        <source>All terms</source>
        <translation>Все слова</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="211">
        </location>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="212">
        </location>
        <source>Query language</source>
        <translation>Язык запроса</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="254">
        </location>
        <source>Simple search</source>
        <translation>Простой поиск</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="254">
        </location>
        <source>History</source>
        <translation>История</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="467">
        </location>
        <source>&lt;html&gt;&lt;head&gt;&lt;style&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="468">
        </location>
        <source>table, th, td {</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="469">
        </location>
        <source>border: 1px solid black;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="470">
        </location>
        <source>border-collapse: collapse;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="471">
        </location>
        <location filename="../ssearch_w.cpp" line="474">
        </location>
        <source>}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="472">
        </location>
        <source>th,td {</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="473">
        </location>
        <source>text-align: center;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="475">
        </location>
        <source>&lt;/style&gt;&lt;/head&gt;&lt;body&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Query language cheat-sheet. In doubt: click &lt;b&gt;Show Query&lt;/b&gt;.&amp;nbsp;</source>
        <translation type="vanished">&lt;p&gt;Шпаргалка по языку запросов. Неясно - щёлкните &lt;b&gt;Показать запрос&lt;/b&gt;.&amp;nbsp;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="477">
        </location>
        <source>You should really look at the manual (F1)&lt;/p&gt;</source>
        <translation>И впрямь стоит заглянуть в справку (F1)&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="478">
        </location>
        <source>&lt;table border=&apos;1&apos; cellspacing=&apos;0&apos;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="479">
        </location>
        <source>&lt;tr&gt;&lt;th&gt;What&lt;/th&gt;&lt;th&gt;Examples&lt;/th&gt;</source>
        <translation>&lt;tr&gt;&lt;th&gt;What&lt;/th&gt;&lt;th&gt;Примеры&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="480">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;And&lt;/td&gt;&lt;td&gt;one two&amp;nbsp;&amp;nbsp;&amp;nbsp;one AND two&amp;nbsp;&amp;nbsp;&amp;nbsp;one &amp;&amp; two&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;И&lt;/td&gt;&lt;td&gt;раз два&amp;nbsp;&amp;nbsp;&amp;nbsp;раз AND два&amp;nbsp;&amp;nbsp;&amp;nbsp;раз &amp;&amp; два&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="481">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Or&lt;/td&gt;&lt;td&gt;one OR two&amp;nbsp;&amp;nbsp;&amp;nbsp;one || two&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Или&lt;/td&gt;&lt;td&gt;раз OR два&amp;nbsp;&amp;nbsp;&amp;nbsp;раз || два&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="482">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Complex boolean. OR has priority, use parentheses&amp;nbsp;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Сложное булево. OR в приоритете, используйте скобки&amp;nbsp;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="483">
        </location>
        <source>where needed&lt;/td&gt;&lt;td&gt;(one AND two) OR three&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>при надобности&lt;/td&gt;&lt;td&gt;(раз AND два) OR три&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="484">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Not&lt;/td&gt;&lt;td&gt;-term&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Не&lt;/td&gt;&lt;td&gt;-слово&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="485">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Phrase&lt;/td&gt;&lt;td&gt;&quot;pride and prejudice&quot;&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Фраза&lt;/td&gt;&lt;td&gt;&quot;гордыня и предубеждение&quot;&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="486">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Ordered proximity (slack=1)&lt;/td&gt;&lt;td&gt;&quot;pride prejudice&quot;o1&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Упорядоченная близость (допуск=1)&lt;/td&gt;&lt;td&gt;&quot;гордыня предубеждение&quot;o1&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="487">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Unordered proximity (slack=1)&lt;/td&gt;&lt;td&gt;&quot;prejudice pride&quot;po1&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Неупорядоченная близость (допуск=1)&lt;/td&gt;&lt;td&gt;&quot;предубеждение гордыня&quot;po1&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="488">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Unordered prox. (default slack=10)&lt;/td&gt;&lt;td&gt;&quot;prejudice&amp;nbsp;pride&quot;p&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Неупор. близ. (штат.допуск=10)&lt;/td&gt;&lt;td&gt;&quot;предубеждение&amp;nbsp;гордыня&quot;p&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="476">
        </location>
        <source>&lt;p&gt;Query language cheat-sheet. In doubt: click &lt;b&gt;Show Query Details&lt;/b&gt;.&amp;nbsp;</source>
        <translation>&lt;p&gt;Шпаргалка по языку запросов. Неясно — щёлкните &lt;b&gt;Показать сведения о запросе&lt;/b&gt;.&amp;nbsp;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="489">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Capitalize to suppress stem expansion&lt;/td&gt;&lt;td&gt;Floor&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Заглавные буквы для подавления словоформ&lt;/td&gt;&lt;td&gt;Слово&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="490">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Field-specific&lt;/td&gt;&lt;td&gt;author:austen&amp;nbsp;&amp;nbsp;title:prejudice&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;По полям&lt;/td&gt;&lt;td&gt;author:остен&amp;nbsp;&amp;nbsp;title:предубеждение&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="491">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;AND inside field (no order)&lt;/td&gt;&lt;td&gt;author:jane,austen&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;AND внутри поля (как угодно)&lt;/td&gt;&lt;td&gt;author:джейн,остен&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="492">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;OR inside field&lt;/td&gt;&lt;td&gt;author:austen/bronte&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;OR внутри поля&lt;/td&gt;&lt;td&gt;author:остен/бронте&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="493">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Field names&lt;/td&gt;&lt;td&gt;title/subject/caption&amp;nbsp;&amp;nbsp;author/from&lt;br&gt;recipient/to&amp;nbsp;&amp;nbsp;filename&amp;nbsp;&amp;nbsp;ext&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Имена полей&lt;/td&gt;&lt;td&gt;title/subject/caption&amp;nbsp;&amp;nbsp;author/from&lt;br&gt;recipient/to&amp;nbsp;&amp;nbsp;filename&amp;nbsp;&amp;nbsp;ext&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="494">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Directory path filter&lt;/td&gt;&lt;td&gt;dir:/home/me&amp;nbsp;&amp;nbsp;dir:doc&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Фильтр путей&lt;/td&gt;&lt;td&gt;dir:/home/me&amp;nbsp;&amp;nbsp;dir:doc&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="495">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;MIME type filter&lt;/td&gt;&lt;td&gt;mime:text/plain mime:video/*&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Фильтр MIME-типов&lt;/td&gt;&lt;td&gt;mime:text/plain mime:video/*&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="496">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Date intervals&lt;/td&gt;&lt;td&gt;date:2018-01-01/2018-31-12&lt;br&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Промежуток времени&lt;/td&gt;&lt;td&gt;date:2018-01-01/2018-31-12&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="497">
        </location>
        <source>date:2018&amp;nbsp;&amp;nbsp;date:2018-01-01/P12M&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>date:2018&amp;nbsp;&amp;nbsp;date:2018-01-01/P12M&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="498">
        </location>
        <source>&lt;tr&gt;&lt;td&gt;Size&lt;/td&gt;&lt;td&gt;size&amp;gt;100k size&amp;lt;1M&lt;/td&gt;&lt;/tr&gt;</source>
        <translation>&lt;tr&gt;&lt;td&gt;Размер&lt;/td&gt;&lt;td&gt;size&amp;gt;100k size&amp;lt;1M&lt;/td&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="499">
        </location>
        <source>&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="503">
        </location>
        <source>Enter file name wildcard expression.</source>
        <translation>Укажите маску имени файла.</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="508">
        </location>
        <source>Enter search terms here.</source>
        <translation>Укажите искомые слова.</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="580">
        </location>
        <source>Bad query string</source>
        <translation>Неверное значение запроса.</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="587">
        </location>
        <source>Out of memory</source>
        <translation>Память исчерпана</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="637">
        </location>
        <source>Can&apos;t open index</source>
        <translation>Не могу открыть индекс</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="661">
        </location>
        <source>Stemming languages for stored query: </source>
        <translation>Языки словоформ для сохранённого запроса: </translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="663">
        </location>
        <location filename="../ssearch_w.cpp" line="674">
        </location>
        <source>differ from current preferences (kept)</source>
        <translation>отличаются от текущих параметров (сохранено)</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="672">
        </location>
        <source>Auto suffixes for stored query: </source>
        <translation>Автоматически подставляемые суффиксы для сохранённого запроса: </translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="682">
        </location>
        <source>Could not restore external indexes for stored query:&lt;br&gt; </source>
        <translation>Не удалось восстановить внешние индексы для значений отбора:&lt;br&gt; </translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="683">
        </location>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="684">
        </location>
        <source>Using current preferences.</source>
        <translation>Используются текущие настройки.</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="695">
        </location>
        <source>Autophrase is set but it was unset for stored query</source>
        <translation>Автофраза задана, но для сохранённого запроса сброшена</translation>
    </message>
    <message>
        <location filename="../ssearch_w.cpp" line="699">
        </location>
        <source>Autophrase is unset but it was set for stored query</source>
        <translation>Автофраза не задана, но для сохранённого запроса задана</translation>
    </message>
</context>
<context>
    <name>SSearchBase</name>
    <message>
        <location filename="../ssearchb.ui" line="14">
        </location>
        <source>SSearchBase</source>
        <translation>SSearchBase</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="43">
        </location>
        <source>Erase search entry</source>
        <translation>Стереть содержимое поиска</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="46">
        </location>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="59">
        </location>
        <source>Start query</source>
        <translation>Начать запрос</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="62">
        </location>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="75">
        </location>
        <source>Choose search type.</source>
        <translation>Выбрать тип поиска.</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="101">
        </location>
        <source>Show query history</source>
        <translation>Показать историю запросов</translation>
    </message>
    <message>
        <location filename="../ssearchb.ui" line="119">
        </location>
        <source>Main menu</source>
        <translation>Главное меню</translation>
    </message>
</context>
<context>
    <name>SearchClauseW</name>
    <message>
        <location filename="../searchclause_w.cpp" line="71">
        </location>
        <source>Any</source>
        <translation>Любые</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="72">
        </location>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="73">
        </location>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="74">
        </location>
        <source>Phrase</source>
        <translation>Фраза</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="75">
        </location>
        <source>Proximity</source>
        <translation>Близость</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="76">
        </location>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="79">
        </location>
        <source>No field</source>
        <translation>Поле отсутствует</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="93">
        </location>
        <source>Select the type of query that will be performed with the words</source>
        <translation>Выберите, какой тип запроса по словам будет произведён</translation>
    </message>
    <message>
        <location filename="../searchclause_w.cpp" line="94">
        </location>
        <source>Number of additional words that may be interspersed with the chosen ones</source>
        <translation>Количество возможных других слов между выбранными</translation>
    </message>
</context>
<context>
    <name>Snippets</name>
    <message>
        <location filename="../snippets.ui" line="14">
        </location>
        <source>Snippets</source>
        <translation>Выдержки</translation>
    </message>
    <message>
        <location filename="../snippets.ui" line="65">
        </location>
        <source>Find:</source>
        <translation>Найти:</translation>
    </message>
    <message>
        <location filename="../snippets.ui" line="88">
        </location>
        <source>Next</source>
        <translation>След.</translation>
    </message>
    <message>
        <location filename="../snippets.ui" line="101">
        </location>
        <source>Prev</source>
        <translation>Пред.</translation>
    </message>
</context>
<context>
    <name>SnippetsW</name>
    <message>
        <location filename="../snippets_w.cpp" line="82">
        </location>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="139">
        </location>
        <location filename="../snippets_w.cpp" line="141">
        </location>
        <location filename="../snippets_w.cpp" line="143">
        </location>
        <location filename="../snippets_w.cpp" line="145">
        </location>
        <location filename="../snippets_w.cpp" line="147">
        </location>
        <location filename="../snippets_w.cpp" line="150">
        </location>
        <location filename="../snippets_w.cpp" line="153">
        </location>
        <location filename="../snippets_w.cpp" line="159">
        </location>
        <location filename="../snippets_w.cpp" line="161">
        </location>
        <location filename="../snippets_w.cpp" line="163">
        </location>
        <location filename="../snippets_w.cpp" line="165">
        </location>
        <location filename="../snippets_w.cpp" line="167">
        </location>
        <location filename="../snippets_w.cpp" line="170">
        </location>
        <location filename="../snippets_w.cpp" line="173">
        </location>
        <source>Snippets Window</source>
        <translation>Выдержки</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="139">
        </location>
        <location filename="../snippets_w.cpp" line="159">
        </location>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="141">
        </location>
        <location filename="../snippets_w.cpp" line="161">
        </location>
        <source>Find (alt)</source>
        <translation>Найти (также)</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="143">
        </location>
        <location filename="../snippets_w.cpp" line="163">
        </location>
        <source>Find next</source>
        <translation>Найти след.</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="145">
        </location>
        <location filename="../snippets_w.cpp" line="165">
        </location>
        <source>Find previous</source>
        <translation>Найти пред.</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="147">
        </location>
        <location filename="../snippets_w.cpp" line="167">
        </location>
        <source>Close window</source>
        <translation>Закрыть окно поиска</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="150">
        </location>
        <location filename="../snippets_w.cpp" line="170">
        </location>
        <source>Increase font size</source>
        <translation type="unfinished">Увеличить размер шрифта</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="153">
        </location>
        <location filename="../snippets_w.cpp" line="173">
        </location>
        <source>Decrease font size</source>
        <translation type="unfinished">Уменьшить размер шрифта</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="181">
        </location>
        <source>Sort By Relevance</source>
        <translation>Сортировать по соответствию</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="183">
        </location>
        <source>Sort By Page</source>
        <translation>Сортировать по странице</translation>
    </message>
    <message>
        <location filename="../snippets_w.cpp" line="254">
        </location>
        <source>&lt;p&gt;Sorry, no exact match was found within limits. Probably the document is very big and the snippets generator got lost in a maze...&lt;/p&gt;</source>
        <translation>&lt;p&gt;К сожалению, точные совпадения с заданными параметрами не найдены. Возможно, документ слишком большой и выдерживалка не выдержала...&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>SpecIdxW</name>
    <message>
        <location filename="../specialindex.ui" line="17">
        </location>
        <source>Special Indexing</source>
        <translation>Специальное индексирование</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="25">
        </location>
        <source>Retry previously failed files.</source>
        <translation>Обработать файлы с ошибками повторно.</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="32">
        </location>
        <source>Else only modified or failed files will be processed.</source>
        <translation>Или будут обрабатываться только изменённые файлы или файлы с ошибками.</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="35">
        </location>
        <source>Erase selected files data before indexing.</source>
        <translation>Стирать сведения по выбранным файлам перед индексированием.</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="56">
        </location>
        <source>Directory to recursively index. This must be inside the regular indexed area&lt;br&gt; as defined in the configuration file (topdirs).</source>
        <translation>Каталог для рекурсивного индексирования. Должен находиться внутри обычной индексируемой области,&lt;br&gt; как указано в файле настройки (topdirs).</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="63">
        </location>
        <location filename="../specialindex.ui" line="122">
        </location>
        <source>Browse</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="73">
        </location>
        <source>Start directory. Must be part of the indexed tree. Use full indexed area if empty.</source>
        <translation>Начальный каталог. Должен быть частью индексируемого дерева каталогов. Использовать весь индекс, если каталог пуст.</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="87">
        </location>
        <source>Leave empty to select all files. You can use multiple space-separated shell-type patterns.&lt;br&gt;Patterns with embedded spaces should be quoted with double quotes.&lt;br&gt;Can only be used if the start target is set.</source>
        <translation>Оставьте поле пустым для выбора всех файлов. Можно использовать несколько шаблонов через пробел.&lt;br&gt;Шаблоны, включающие в себя пробел, должны быть взяты в двойные кавычки.&lt;br&gt;Можно использовать только если задан начальный каталог.</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="90">
        </location>
        <source>Selection patterns:</source>
        <translation>Шаблоны отбора:</translation>
    </message>
    <message>
        <location filename="../specialindex.ui" line="132">
        </location>
        <source>Diagnostics output file. Will be truncated and receive indexing diagnostics (reasons for files not being indexed).</source>
        <translation>Файл с выводом диагностических сообщений. Будет пересоздан с дальнейшей записью диагностики индексирования (причин пропуска файлов).</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="436">
        </location>
        <source>Top indexed entity</source>
        <translation>Верхняя индексируемая сущность</translation>
    </message>
    <message>
        <location filename="../rclm_idx.cpp" line="442">
        </location>
        <source>Diagnostics file</source>
        <translation>Файл журнала</translation>
    </message>
</context>
<context>
    <name>SpellBase</name>
    <message>
        <location filename="../spell.ui" line="26">
        </location>
        <source>Term Explorer</source>
        <translation>Обозреватель терминов</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="49">
        </location>
        <source>Match</source>
        <translation>Учитывать</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="56">
        </location>
        <source>Case</source>
        <translation>регистр</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="63">
        </location>
        <source>Accents</source>
        <translation>диакритические знаки</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="79">
        </location>
        <source>&amp;Expand </source>
        <translation>&amp;Однокоренные слова </translation>
    </message>
    <message>
        <location filename="../spell.ui" line="82">
        </location>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="95">
        </location>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="98">
        </location>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../spell.ui" line="112">
        </location>
        <source>No db info.</source>
        <translation>Нет информации о базе.</translation>
    </message>
</context>
<context>
    <name>SpellW</name>
    <message>
        <location filename="../spell_w.cpp" line="65">
        </location>
        <source>Wildcards</source>
        <translation>маски</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="67">
        </location>
        <source>Regexp</source>
        <translation>Регулярные выражения</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="69">
        </location>
        <source>Stem expansion</source>
        <translation>Однокоренные слова</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="71">
        </location>
        <source>Spelling/Phonetic</source>
        <translation>Написание/произношение</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="73">
        </location>
        <source>Show index statistics</source>
        <translation>Показать статистику индекса</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="75">
        </location>
        <source>List files which could not be indexed (slow)</source>
        <translation>Показать непроиндексировавшиеся файлы (небыстро)</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="83">
        </location>
        <source>error retrieving stemming languages</source>
        <translation>ошибка получения списка языков словоформ</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="183">
        </location>
        <source>Index: %1 documents, average length %2 terms.%3 results</source>
        <translation>Индекс: %1 документ(ов), средняя длина %2 слов(о). %3 результат(ов)</translation>
    </message>
    <message>
        <source>Spell expansion error. </source>
        <translation type="vanished">Ошибка поиска однокоренных слов. </translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="196">
        </location>
        <source>Spell expansion error.</source>
        <translation>Ошибка поиска однокоренных слов.</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="201">
        </location>
        <source>%1 results</source>
        <translation>%1 результат(ов)</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="220">
        </location>
        <source>No expansion found</source>
        <translation>Однокоренных слов не найдено</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="229">
        </location>
        <source>List was truncated alphabetically, some frequent </source>
        <translation>Список сокращён по алфавиту, некоторые часто повторяющиеся </translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="234">
        </location>
        <source>terms may be missing. Try using a longer root.</source>
        <translation>слова могут отсутствовать. Попробуйте более длинный корень.</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="269">
        </location>
        <source>Number of documents</source>
        <translation>Число документов</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="275">
        </location>
        <source>Average terms per document</source>
        <translation>Слов на документ (в среднем)</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="281">
        </location>
        <source>Smallest document length (terms)</source>
        <translation>Наименьшая длина документа (слов)</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="287">
        </location>
        <source>Longest document length (terms)</source>
        <translation>Наибольшая длина документа (слов)</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="299">
        </location>
        <source>Results from last indexing:</source>
        <translation>Результаты последнего индексирования:</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="303">
        </location>
        <source>Documents created/updated</source>
        <translation>Создано/обновлено документов</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="308">
        </location>
        <source>Files tested</source>
        <translation>Проверено файлов</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="313">
        </location>
        <source>Unindexed files</source>
        <translation>Непроиндексированных файлов</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="325">
        </location>
        <source>Database directory size</source>
        <translation>Размер каталога базы данных</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="346">
        </location>
        <source>MIME types:</source>
        <translation>Типы MIME:</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="435">
        </location>
        <source>Item</source>
        <translation>Элемент</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="436">
        </location>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="443">
        </location>
        <source>Term</source>
        <translation>Термин</translation>
    </message>
    <message>
        <location filename="../spell_w.cpp" line="444">
        </location>
        <source>Doc. / Tot.</source>
        <translation>Док. / Всего</translation>
    </message>
</context>
<context>
    <name>UIPrefsDialog</name>
    <message>
        <location filename="../uiprefs_w.cpp" line="90">
        </location>
        <source>Any term</source>
        <translation>Любой</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="91">
        </location>
        <source>All terms</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="92">
        </location>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="93">
        </location>
        <source>Query language</source>
        <translation>Язык запросов</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="94">
        </location>
        <source>Value from previous program exit</source>
        <translation>Значение из предыдущего запуска программы</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="257">
        </location>
        <location filename="../uiprefs_w.cpp" line="266">
        </location>
        <location filename="../uiprefs_w.cpp" line="312">
        </location>
        <location filename="../uiprefs_w.cpp" line="662">
        </location>
        <location filename="../uiprefs_w.cpp" line="671">
        </location>
        <source>Choose</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="284">
        </location>
        <source>error retrieving stemming languages</source>
        <translation>ошибка получения списка языков словоформ</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="345">
        </location>
        <source>Context</source>
        <translation>Контекст</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="346">
        </location>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="347">
        </location>
        <source>Shortcut</source>
        <translation>Сочетание клавиш</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="348">
        </location>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="396">
        </location>
        <source>Default QtWebkit font</source>
        <translation>Шрифт QtWebkit по умолчанию</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="554">
        </location>
        <source>Result list paragraph format (erase all to reset to default)</source>
        <translation>Формат абзаца в списке результатов (очистите для сброса к умолчанию)</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="565">
        </location>
        <source>Result list header (default is empty)</source>
        <translation>Заголовок списка результатов (по умолчанию пуст)</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="624">
        </location>
        <source>Choose QSS File</source>
        <translation>Выбрать файл QSS</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="728">
        </location>
        <source>At most one index should be selected</source>
        <translation>Следует выбрать не больше одного индекса</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="791">
        </location>
        <source>Select recoll config directory or xapian index directory (e.g.: /home/me/.recoll or /home/me/.recoll/xapiandb)</source>
        <translation>Выберите каталог настроек recoll или каталог индекса xapian (например: /home/me/.recoll или /home/me/.recoll/xapiandb)</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="805">
        </location>
        <source>The selected directory looks like a Recoll configuration directory but the configuration could not be read</source>
        <translation>Выбранный каталог выглядит как каталог с настройками Recoll, но настройки не выходит прочитать</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="815">
        </location>
        <source>The selected directory does not appear to be a Xapian index</source>
        <translation>Выбранный каталог непохож на индекс Xapian</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="821">
        </location>
        <source>Can&apos;t add index with different case/diacritics stripping option.</source>
        <translation>Невозможно добавить индекс с другими настройками учёта регистра и диакритических знаков.</translation>
    </message>
    <message>
        <source>Cant add index with different case/diacritics stripping option</source>
        <translation type="vanished">Невозможно добавить индекс с другими настройками учёта регистра и диакритических знаков</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="826">
        </location>
        <source>This is the main/local index!</source>
        <translation>Этот индекс является главным/локальным!</translation>
    </message>
    <message>
        <location filename="../uiprefs_w.cpp" line="835">
        </location>
        <source>The selected directory is already in the index list</source>
        <translation>Этот каталог уже указан в списке индексов</translation>
    </message>
</context>
<context>
    <name>ViewAction</name>
    <message>
        <location filename="../viewaction_w.cpp" line="88">
        </location>
        <source>Desktop Default</source>
        <translation>Взять из окружения</translation>
    </message>
    <message>
        <location filename="../viewaction_w.cpp" line="92">
        </location>
        <source>MIME type</source>
        <translation>Тип MIME</translation>
    </message>
    <message>
        <location filename="../viewaction_w.cpp" line="93">
        </location>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../viewaction_w.cpp" line="176">
        </location>
        <source>Changing entries with different current values</source>
        <translation>Изменение записей с различными текущими значениями</translation>
    </message>
</context>
<context>
    <name>ViewActionBase</name>
    <message>
        <location filename="../viewaction.ui" line="14">
        </location>
        <source>Native Viewers</source>
        <translation>Встроенные просмотрщики</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="20">
        </location>
        <source>Select one or several mime types then use the controls in the bottom frame to change how they are processed.</source>
        <translation>Выберите MIME-типы и используйте кнопки в рамке ниже, чтобы изменить характер их обработки.</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="30">
        </location>
        <source>Use Desktop preferences by default</source>
        <translation>Использовать настройки окружения по умолчанию</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="37">
        </location>
        <source>Select one or several file types, then use the controls in the frame below to change how they are processed</source>
        <translation>Выберите типы файлов и используйте кнопки, расположенные в рамке ниже, чтобы изменить характер их обработки</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="90">
        </location>
        <source>Recoll action:</source>
        <translation>Действие Recoll:</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="109">
        </location>
        <source>current value</source>
        <translation>текущее значение</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="122">
        </location>
        <source>Select same</source>
        <translation>Выделить такие же</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="142">
        </location>
        <source>&lt;b&gt;New Values:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Новые значение:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="149">
        </location>
        <source>Exception to Desktop preferences</source>
        <translation>Исключения из настроек окружения</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="158">
        </location>
        <source>Action (empty -&gt; recoll default)</source>
        <translation>Действие (пусто -&gt; по умолчанию)</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="170">
        </location>
        <source>Apply to current selection</source>
        <translation>Применить к выделению</translation>
    </message>
    <message>
        <location filename="../viewaction.ui" line="197">
        </location>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>Webcache</name>
    <message>
        <location filename="../webcache.ui" line="14">
        </location>
        <source>Webcache editor</source>
        <translation>Редактор веб-кэша</translation>
    </message>
    <message>
        <location filename="../webcache.ui" line="20">
        </location>
        <source>TextLabel</source>
        <translation>Текстовая подпись</translation>
    </message>
    <message>
        <location filename="../webcache.ui" line="29">
        </location>
        <source>Search regexp</source>
        <translation>Поиск по регулярному выражению</translation>
    </message>
</context>
<context>
    <name>WebcacheEdit</name>
    <message>
        <location filename="../webcache.cpp" line="280">
        </location>
        <source>Maximum size %1 (Index config.). Current size %2. Write position %3.</source>
        <translation>Предельный размер %1 (конф. индекса). Текущий размер %2. Позиция записи %3.</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="295">
        </location>
        <source>Copy URL</source>
        <translation>Копировать URL</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="296">
        </location>
        <source>Save to File</source>
        <translation>Сохранить в файл</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="302">
        </location>
        <source>Unknown indexer state. Can&apos;t edit webcache file.</source>
        <translation>Неизвестный статус индексатора. Невозможно редактировать файл веб-кэша.</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="306">
        </location>
        <source>Indexer is running. Can&apos;t edit webcache file.</source>
        <translation>Индексатор запущен. Нельзя редактировать файл веб-кэша.</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="309">
        </location>
        <source>Delete selection</source>
        <translation>Удалить выделенные</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="354">
        </location>
        <source>File creation failed: </source>
        <translation>Создание файла не удалось: </translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="374">
        </location>
        <source>Webcache was modified, you will need to run the indexer after closing this window.</source>
        <translation>Содержимое веб-кэша былыо изменено, после закрытия этого окна необходимо запустить индексирование.</translation>
    </message>
</context>
<context>
    <name>WebcacheModel</name>
    <message>
        <location filename="../webcache.cpp" line="181">
        </location>
        <source>MIME</source>
        <translation>MIME</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="182">
        </location>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="183">
        </location>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../webcache.cpp" line="184">
        </location>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Url</source>
        <translation type="vanished">Ссылка</translation>
    </message>
</context>
<context>
    <name>WinSchedToolW</name>
    <message>
        <location filename="../winschedtool.ui" line="14">
        </location>
        <source>Recoll Batch indexing</source>
        <translation>Пакетное индексирование Recoll</translation>
    </message>
    <message>
        <location filename="../winschedtool.ui" line="41">
        </location>
        <source>Start Windows Task Scheduler tool</source>
        <translation>Запустить планировщик задач Windows</translation>
    </message>
    <message>
        <location filename="../winschedtool.cpp" line="41">
        </location>
        <location filename="../winschedtool.cpp" line="64">
        </location>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../winschedtool.cpp" line="42">
        </location>
        <source>Configuration not initialized</source>
        <translation>Конфигурация не инициализирована</translation>
    </message>
    <message>
        <location filename="../winschedtool.cpp" line="65">
        </location>
        <source>Could not create batch file</source>
        <translation>Не удалось создать пакетный файл</translation>
    </message>
    <message>
        <location filename="../winschedtool.cpp" line="69">
        </location>
        <source>&lt;h3&gt;Recoll indexing batch scheduling&lt;/h3&gt;&lt;p&gt;We use the standard Windows task scheduler for this. The program will be started when you click the button below.&lt;/p&gt;&lt;p&gt;You can use either the full interface (&lt;i&gt;Create task&lt;/i&gt; in the menu on the right), or the simplified &lt;i&gt;Create Basic task&lt;/i&gt; wizard. In both cases Copy/Paste the batch file path listed below as the &lt;i&gt;Action&lt;/i&gt; to be performed.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Планирование пакетного индексирования Recoll&lt;/h3&gt;&lt;p&gt;Для этого используется стандартный планировщик задач Windows. Программа будет запущена после нажатия расположенной ниже кнопки.&lt;/p&gt;&lt;p&gt;Можно использовать либо полный интерфейс (&lt;i&gt;Создать задачу&lt;/i&gt; в меню справа), либо упрощённый мастер &lt;i&gt;Создать простую задачу&lt;/i&gt;. В обоих случаях следует Копировать/Вставить приведённый ниже путь к пакетному файлу как &lt;i&gt;Действие&lt;/i&gt; для выполнения.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../winschedtool.cpp" line="93">
        </location>
        <source>Command already started</source>
        <translation>Команда уже запущена</translation>
    </message>
</context>
<context>
    <name>confgui::ConfParamFNW</name>
    <message>
        <location filename="../confgui/confgui.cpp" line="614">
        </location>
        <source>Choose</source>
        <translation>Выбрать</translation>
    </message>
</context>
<context>
    <name>confgui::ConfParamSLW</name>
    <message>
        <location filename="../confgui/confgui.cpp" line="689">
        </location>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../confgui/confgui.cpp" line="691">
        </location>
        <source>Add entry</source>
        <translation>Добавить запись</translation>
    </message>
    <message>
        <location filename="../confgui/confgui.cpp" line="699">
        </location>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../confgui/confgui.cpp" line="701">
        </location>
        <source>Delete selected entries</source>
        <translation>Удалить выделенные записи</translation>
    </message>
    <message>
        <location filename="../confgui/confgui.cpp" line="709">
        </location>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location filename="../confgui/confgui.cpp" line="711">
        </location>
        <source>Edit selected entries</source>
        <translation>Изменить выделенные записи</translation>
    </message>
</context>
<context>
    <name>uiPrefsDialogBase</name>
    <message>
        <location filename="../uiprefs.ui" line="14">
        </location>
        <source>Recoll - User Preferences</source>
        <translation>Recoll — настройки пользователя</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="27">
        </location>
        <source>User interface</source>
        <translation>Интерфейс пользователя</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="48">
        </location>
        <source>Choose editor applications</source>
        <translation>Выбор приложений-редакторов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="77">
        </location>
        <source>Start with simple search mode: </source>
        <translation>Начать с режима простого поиска: </translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="113">
        </location>
        <source>Limit the size of the search history. Use 0 to disable, -1 for unlimited.</source>
        <translation>Ограничения размера истории поиска. 0: отключить, -1: без ограничений.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="116">
        </location>
        <source>Maximum size of search history (0: disable, -1: unlimited):</source>
        <translation>Предельный размер истории поиска (0: отключить, -1: без ограничений):</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="154">
        </location>
        <source>Start with advanced search dialog open.</source>
        <translation>Открывать диалог сложного поиска при запуске.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="164">
        </location>
        <source>Remember sort activation state.</source>
        <translation>Запомнить порядок сортировки результатов.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="185">
        </location>
        <source>Depth of side filter directory tree</source>
        <translation>Глубина бокового дерева каталогов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="225">
        </location>
        <source>See Qt QDateTimeEdit documentation. E.g. yyyy-MM-dd. Leave empty to use the default Qt/System format.</source>
        <translation type="unfinished">См. документацию Qt QDateTimeEdit. Например, yyyy-MM-dd. Оставьте пустым, чтобы использовать формат по умолчанию Qt/System.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="228">
        </location>
        <source>Side filter dates format (change needs restart)</source>
        <translation type="unfinished">Формат даты для бокового фильтра (изменение требует перезапуска)</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="272">
        </location>
        <source>Decide if document filters are shown as radio buttons, toolbar combobox, or menu.</source>
        <translation>Стиль отображения фильтров: в виде селекторов, поля со списком на панели инструментов или меню.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="275">
        </location>
        <source>Document filter choice style:</source>
        <translation>Стиль отображения фильтров:</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="282">
        </location>
        <source>Buttons Panel</source>
        <translation>Панель кнопок</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="292">
        </location>
        <source>Toolbar Combobox</source>
        <translation>Поле со списком</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="302">
        </location>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="316">
        </location>
        <source>Hide some user interface elements.</source>
        <translation>Скрыть некоторые элементы интерфейса</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="319">
        </location>
        <source>Hide:</source>
        <translation>Скрыть:</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="326">
        </location>
        <source>Toolbars</source>
        <translation>Поля со списками</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="336">
        </location>
        <source>Status bar</source>
        <translation>Панель состояния</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="346">
        </location>
        <source>Show button instead.</source>
        <translation>Показывать кнопки.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="349">
        </location>
        <source>Menu bar</source>
        <translation>Панель меню</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="359">
        </location>
        <source>Show choice in menu only.</source>
        <translation>Отображать выбор только в меню.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="362">
        </location>
        <source>Simple search type</source>
        <translation>Тип простого поиска</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="372">
        </location>
        <source>Clear/Search buttons</source>
        <translation>Кнопки Очистить/Поиск</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="386">
        </location>
        <source>Show system tray icon.</source>
        <translation>Отображать значок в трее</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="396">
        </location>
        <source>Close to tray instead of exiting.</source>
        <translation>Сворачивать окно вместо закрытия.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="406">
        </location>
        <source>Generate desktop notifications.</source>
        <translation>Отображать уведомления.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="418">
        </location>
        <source>Suppress all beeps.</source>
        <translation>Отключить звук.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="428">
        </location>
        <source>Show warning when opening temporary file.</source>
        <translation>Отображать предупреждение при открытии временного файла.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="440">
        </location>
        <source>Disable Qt autocompletion in search entry.</source>
        <translation>Отключить автодополнение Qt в строке поиска.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="465">
        </location>
        <source>Start search on completer popup activation.</source>
        <translation>Начинать поиск при активации всплывающего окна автодополнения.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="485">
        </location>
        <source>Maximum number of history entries in completer list</source>
        <translation>Предельное число записей журнала в списке автодополнения</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="488">
        </location>
        <source>Number of history entries in completer:</source>
        <translation>Число записей журнала в автодополнении:</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="526">
        </location>
        <source>Displays the total number of occurences of the term in the index</source>
        <translation>Показывает общее количество вхождений термина в индексе</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="529">
        </location>
        <source>Show hit counts in completer popup.</source>
        <translation>Показывать счётчик нажатий во всплывающем окне автодополнения.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1152">
        </location>
        <source>Texts over this size will not be highlighted in preview (too slow).</source>
        <translation>Текст большего размера не будет подсвечен при просмотре (слишком медленно).</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1155">
        </location>
        <source>Maximum text size highlighted for preview (kilobytes)</source>
        <translation>Предельный размер текста, выбранного для просмотра (в килобайтах)</translation>
    </message>
    <message>
        <source>Prefer Html to plain text for preview.</source>
        <translation type="vanished">Предпочитать для просмотра HTML простому тексту.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1196">
        </location>
        <source>Prefer HTML to plain text for preview.</source>
        <translation>Предпочитать для просмотра HTML простому тексту.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1206">
        </location>
        <source>Make links inside the preview window clickable, and start an external browser when they are clicked.</source>
        <translation>Сделать ссылки внутри окна просмотра активными и запускать браузер при щелчке по ссылке.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1209">
        </location>
        <source>Activate links in preview.</source>
        <translation>Активировать ссылки в режиме просмотра.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1270">
        </location>
        <source>Lines in PRE text are not folded. Using BR loses some indentation. PRE + Wrap style may be what you want.</source>
        <translation>Строки в тексте PRE нескладные. При использовании BR теряются некоторые отступы. Возможно, PRE + Wrap подойдёт больше.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1273">
        </location>
        <source>Plain text to HTML line style</source>
        <translation>Стиль отображения строк HTML в простом тексте</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1280">
        </location>
        <source>&lt;BR&gt;</source>
        <translation>&lt;BR&gt;</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1290">
        </location>
        <source>&lt;PRE&gt;</source>
        <translation>&lt;PRE&gt;</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1300">
        </location>
        <source>&lt;PRE&gt; + wrap</source>
        <translation>&lt;PRE&gt; + wrap</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="550">
        </location>
        <source>Highlight CSS style for query terms</source>
        <translation>CSS-стиль подсветки слов запроса</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="566">
        </location>
        <source>Query terms highlighting in results. &lt;br&gt;Maybe try something like &quot;color:red;background:yellow&quot; for something more lively than the default blue...</source>
        <translation>Подсветка терминов в результатах запроса. &lt;br&gt;Можно попробовать что-то более выделяющееся вроде &quot;color:red;background:yellow&quot; вместо цвета по умолчанию...</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="589">
        </location>
        <source>Zoom factor for the user interface. Useful if the default is not right for your screen resolution.</source>
        <translation>Коэффициент масштабирования интерфейса. Полезно, если не устраивает значение по умолчанию.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="592">
        </location>
        <source>Display scale (default 1.0):</source>
        <translation>Размер отображения (по умолчанию 1.0):</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="629">
        </location>
        <source>Application Qt style sheet</source>
        <translation>Внешний вид приложения</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="639">
        </location>
        <source>Resets the style sheet to default</source>
        <translation>Сбросить на вид по умолчению</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="642">
        </location>
        <source>None (default)</source>
        <translation>Нет (по умолчанию)</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="649">
        </location>
        <source>Uses the default dark mode style sheet</source>
        <translation>Использовать тёмную тему по умолчанию</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="652">
        </location>
        <source>Dark mode</source>
        <translation>Тёмная тема</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="659">
        </location>
        <source>Opens a dialog to select the style sheet file.&lt;br&gt;Look at /usr/share/recoll/examples/recoll[-dark].qss for an example.</source>
        <translation>Откроется диалоговое окно для выбора файла стиля оформления.&lt;br&gt; Для примера откройте /usr/share/recoll/examples/recoll[-dark].qss</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="662">
        </location>
        <source>Choose QSS File</source>
        <translation>Выбрать файл QSS</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1375">
        </location>
        <source>Shortcuts</source>
        <translation>Сочетания клавиш</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1388">
        </location>
        <source>Use F1 to  access the manual</source>
        <translation>Для вызова справки нажмите F1</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1408">
        </location>
        <source>Reset shortcuts defaults</source>
        <translation>Сбросить настройки сочетаний клавиш</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="738">
        </location>
        <source>Result List</source>
        <translation>Список результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="62">
        </location>
        <source>If set, starting a new instance on the same index will raise an existing one.</source>
        <translation type="unfinished">Если установлено, запуск нового экземпляра на том же индексе вызовет существующий.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="65">
        </location>
        <source>Single application</source>
        <translation type="unfinished">Одно приложение</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="182">
        </location>
        <source>Set to 0 to disable and speed up startup by avoiding tree computation.</source>
        <translation type="unfinished">Установите значение 0, чтобы отключить и ускорить запуск, избегая вычисления дерева.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="450">
        </location>
        <source>The completion only changes the entry when activated.</source>
        <translation type="unfinished">Завершение изменяет запись только при активации.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="453">
        </location>
        <source>Completion: no automatic line editing.</source>
        <translation type="unfinished">Завершение: нет автоматического редактирования строки.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="702">
        </location>
        <source>Interface language (needs restart):</source>
        <translation type="unfinished">Язык интерфейса (требуется перезагрузка):</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="705">
        </location>
        <source>Note: most translations are incomplete. Leave empty to use the system environment.</source>
        <translation type="unfinished">Примечание: большинство переводов неполные. Оставьте пустым, чтобы использовать системную среду.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="754">
        </location>
        <source>Number of entries in a result page</source>
        <translation>Число записей в списке результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="794">
        </location>
        <source>Result list font</source>
        <translation>Шрифт списка результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="804">
        </location>
        <source>Opens a dialog to select the result list font</source>
        <translation>Открыть диалоговое окно выбора шрифта списка результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="807">
        </location>
        <source>Helvetica-10</source>
        <translation>Helvetica-10</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="814">
        </location>
        <source>Resets the result list font to the system default</source>
        <translation>Сбросить шрифт списка результатов на системный</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="817">
        </location>
        <location filename="../uiprefs.ui" line="946">
        </location>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="841">
        </location>
        <source>Edit result paragraph format string</source>
        <translation>Редактировать строку форматирования параграфа результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="850">
        </location>
        <source>Edit result page html header insert</source>
        <translation>Редактировать вставку html-заголовка страницы результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="859">
        </location>
        <source>Date format (strftime(3))</source>
        <translation>Формат даты (strftime(3))</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="883">
        </location>
        <source>Abstract snippet separator</source>
        <translation>Условный разделитель выдержек</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="920">
        </location>
        <source>User style to apply to the snippets window.&lt;br&gt; Note: the result page header insert is also included in the snippets window header.</source>
        <translation>Пользовательский стиль оформления для окна просмотра выдержек.&lt;br&gt; На заметку: вставка заголовка страницы результатов также включена в заголовок окна выдержек.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="923">
        </location>
        <source>Snippets window CSS file</source>
        <translation>Файл оформления CSS окна просмотра выдержек</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="933">
        </location>
        <source>Opens a dialog to select the Snippets window CSS style sheet file</source>
        <translation>Откроется окно выбора файла оформления CSS окна просмотра выдержек</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="936">
        </location>
        <location filename="../uiprefs.ui" line="1814">
        </location>
        <source>Choose</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="943">
        </location>
        <source>Resets the Snippets window style</source>
        <translation>Сбросить стиль оформления окна просмотра выдержек</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="976">
        </location>
        <source>Maximum number of snippets displayed in the snippets window</source>
        <translation>Предельное число выдержек, показываемых в окне просмотра</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1017">
        </location>
        <source>Sort snippets by page number (default: by weight).</source>
        <translation>Сортировать по номеру страницы (по умолчанию: по объёму)</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1027">
        </location>
        <source>Display a Snippets link even if the document has no pages (needs restart).</source>
        <translation>Показать ссылку на выдержки, даже если в документе нет страниц (нужен перезапуск).</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1053">
        </location>
        <source>Result Table</source>
        <translation>Таблица результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1059">
        </location>
        <source>Hide result table header.</source>
        <translation>Скрывать шапку таблицы результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1069">
        </location>
        <source>Show result table row headers.</source>
        <translation>Показывать шапки рядов таблицы результатов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1079">
        </location>
        <source>Disable the Ctrl+[0-9]/Shift+[a-z] shortcuts for jumping to table rows.</source>
        <translation>Отключить сочетания клавиш Ctrl+[0-9]/Shift+[a-z] для перехода по рядам таблицы.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1091">
        </location>
        <source>To display document text instead of metadata in result table detail area, use:</source>
        <translation>Для отображения в таблице результатов текста документа вместо метаданных используйте: </translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1098">
        </location>
        <source>left mouse click</source>
        <translation>Щелчок левой кнопкой мыши</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1105">
        </location>
        <source>Shift+click</source>
        <translation>Shift+щелчок</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1114">
        </location>
        <source>Do not display metadata when hovering over rows.</source>
        <translation>Не показывать метаданные при наведении курсора на строки.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1138">
        </location>
        <source>Preview</source>
        <translation type="unfinished">Просмотр</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1227">
        </location>
        <source>Set to 0 to disable details/summary feature</source>
        <translation type="unfinished">Установите значение 0, чтобы отключить функцию деталей/сводки.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1230">
        </location>
        <source>Fields display: max field length before using summary:</source>
        <translation type="unfinished">Поля отображения: максимальная длина поля перед использованием резюме:</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1320">
        </location>
        <source>Number of lines to be shown over a search term found by preview search.</source>
        <translation type="unfinished">Количество строк, которые будут показаны над найденным поисковым запросом в предварительном просмотре.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1323">
        </location>
        <source>Search term line offset:</source>
        <translation type="unfinished">Смещение строки поискового запроса:</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1420">
        </location>
        <source>Search parameters</source>
        <translation>Параметры поиска</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1426">
        </location>
        <source>If checked, results with the same content under different names will only be shown once.</source>
        <translation>Показывать результаты с тем же содержанием под разными именами не более одного раза</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1429">
        </location>
        <source>Hide duplicate results.</source>
        <translation>Скрывать дубликаты.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1443">
        </location>
        <source>Stemming language</source>
        <translation>Язык словоформ</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1481">
        </location>
        <source>A search for [rolling stones] (2 terms) will be changed to [rolling or stones or (rolling phrase 2 stones)]. 
This should give higher precedence to the results where the search terms appear exactly as entered.</source>
        <translation>Поиск [rolling stones] (два слова) будет изменён на [rolling OR stones OR (rolling phrase 2 stones)].</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1485">
        </location>
        <source>Automatically add phrase to simple searches</source>
        <translation>Автоматически добавлять фразу при простом поиске</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1500">
        </location>
        <source>Frequency percentage threshold over which we do not use terms inside autophrase. 
Frequent terms are a major performance issue with phrases. 
Skipped terms augment the phrase slack, and reduce the autophrase efficiency.
The default value is 2 (percent). </source>
        <translation>Порог частоты в процентах, выше которого слова в автофразе не используются. 
Часто появляющиеся слова представляют основную проблему обработки фраз. 
Пропуск слов ослабляет фразу и уменьшает эффективность функции автофразы. 
Значение по умолчанию: 2 (процента). </translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1506">
        </location>
        <source>Autophrase term frequency threshold percentage</source>
        <translation>Процент порогового значения частоты автофраз</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1551">
        </location>
        <source>Do we try to build abstracts for result list entries by using the context of query terms ? 
May be slow for big documents.</source>
        <translation>Создавать описания для результатов поиска с использованием контекста слов запроса?
Процесс может оказаться медленным для больших документов.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1555">
        </location>
        <source>Dynamically build abstracts</source>
        <translation>Составлять описания на лету</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1562">
        </location>
        <source>Do we synthetize an abstract even if the document seemed to have one?</source>
        <translation>Создавать описание, даже когда оно вроде есть для данного документа?</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1565">
        </location>
        <source>Replace abstracts from documents</source>
        <translation>Заменять описания из документов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1580">
        </location>
        <source>Synthetic abstract size (characters)</source>
        <translation>Размер создаваемого описания (символов)</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1635">
        </location>
        <source>Synthetic abstract context words</source>
        <translation>Число слов контекста в описании</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1691">
        </location>
        <source>The words in the list will be automatically turned to ext:xxx clauses in the query language entry.</source>
        <translation>Список слов, которые будут автоматически преобразованы в расширение файла вида ext:xxx в запросе.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1694">
        </location>
        <source>Query language magic file name suffixes.</source>
        <translation>Распознавание типа файлов при помощи файла сигнатур (magic file).</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1704">
        </location>
        <location filename="../uiprefs.ui" line="1801">
        </location>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1738">
        </location>
        <source>Add common spelling approximations for rare terms.</source>
        <translation>Добавлять наиболее подходящие варианты написания для редко встречающихся терминов.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1741">
        </location>
        <source>Automatic spelling approximation.</source>
        <translation>Автоподбор написания.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1748">
        </location>
        <source>Max spelling distance</source>
        <translation>Предельное расстояние между написаниями</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1791">
        </location>
        <source>Synonyms file</source>
        <translation>Файл с синонимами</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1836">
        </location>
        <source>Wild card characters *?[] will processed as punctuation instead of being expanded</source>
        <translation type="unfinished">Символы подстановки *?[] будут обработаны как знаки препинания, а не как расширенные символы.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1839">
        </location>
        <source>Ignore wild card characters in ALL terms and ANY terms modes</source>
        <translation type="unfinished">Игнорировать символы подстановки в режимах ВСЕ термины и ЛЮБЫЕ термины.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1868">
        </location>
        <source>External Indexes</source>
        <translation>Индексы однокоренных слов</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1885">
        </location>
        <source>Toggle selected</source>
        <translation>Переключить выделенные</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1892">
        </location>
        <source>Activate All</source>
        <translation>Включить всё</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1899">
        </location>
        <source>Deactivate All</source>
        <translation>Выключить всё</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1906">
        </location>
        <source>Set path translations for the selected index or for the main one if no selection exists.</source>
        <translation>Задать корректировку путей для выбранного или главного индекса, если ничего не выбрано.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1909">
        </location>
        <source>Paths translations</source>
        <translation>Корректировка путей</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1930">
        </location>
        <source>Remove from list. This has no effect on the disk index.</source>
        <translation>Удалить из списка. Индекс на диске без изменений.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1933">
        </location>
        <source>Remove selected</source>
        <translation>Удалить выделенное</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1959">
        </location>
        <source>Click to add another index directory to the list. You can select either a Recoll configuration directory or a Xapian index.</source>
        <translation>Щёлкните, чтобы добавить другой каталог индекса в список. Можно выбрать каталог конфигурации Recoll или индекс Xapian.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1962">
        </location>
        <source>Add index</source>
        <translation>Добавить индекс</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1974">
        </location>
        <source>Misc</source>
        <translation>Разное</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1980">
        </location>
        <source>The bug causes a strange circle characters to be displayed inside highlighted Tamil words. The workaround inserts an additional space character which appears to fix the problem.</source>
        <translation>Ошибка, приводящая к отображению странных символов в подсвеченных словах на тамильском. Обход заключается в том, чтобы вставить дополнительный символ пробела.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="1983">
        </location>
        <source>Work around Tamil QTBUG-78923 by inserting space before anchor text</source>
        <translation>Обходить ошибку QTBUG-78923 в записях на тамильском, вставляя пробел перед якорным текстом.</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="2025">
        </location>
        <source>Apply changes</source>
        <translation>Принять изменения</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="2028">
        </location>
        <source>&amp;OK</source>
        <translation>&amp;ОК</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="2044">
        </location>
        <source>Discard changes</source>
        <translation>Отменить изменения</translation>
    </message>
    <message>
        <location filename="../uiprefs.ui" line="2047">
        </location>
        <source>&amp;Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
</TS>
